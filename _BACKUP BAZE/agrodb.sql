-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 27, 2017 at 11:42 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `agrodb`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_05_12_181805_DodajPodatkeZaUsere', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('belteksllc@gmail.com', '$2y$10$ziLFyPWA8RBV1Z1JMFIP7.ZJBQhPuVqKg46xVoLgejQBlnd7cArne', '2017-05-25 08:11:42');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `surname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `streetandnumber` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postalcode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Srbija',
  `phonenumber` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isActive` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `surname`, `streetandnumber`, `postalcode`, `city`, `country`, `phonenumber`, `role`, `isActive`) VALUES
(1, 'Administrator Novi', 'belteksllc@gmail.com', '$2y$10$.wJ6L4o58xhBUt4S8e7Bj.S8tf28uE/jC7.84WhbuNQame4S8e5Fy', 'KluRY60bBCZkzzbITEdgcDkw1I4Mh5nj5NYS8opdCpv2eNdvVHYCNrV8uhzd', '2017-05-19 16:08:55', '2017-05-23 17:16:11', 'Kizici', 'X Y 20', '11080', 'Zemun', 'Srbija', '011123456', 'admin', 1),
(2, 'Stasic', 'stasavujicic@gmail.com', '$2y$10$367J.fID88wNkK.lv7ldj.2jFptxgjGyGf3M84exLiJx3e2.LuJby', 'UmkMtkhqC4ED8pS0JYuzJsVkfSGi3Luf4CojXTIA2ZCZbr86uGhmF37FVOKr', '2017-05-21 12:33:31', '2017-05-23 17:46:23', 'Stankovic', 's s10', '11000', 'Bgd', 'Srb', '011123456', 'user', 0),
(3, 'Stasic', 'stasa@matf.bg.ac.rs', '$2y$10$glczdiumlxw/j/7SB6HFVOo8G9E0rN4.GCCY6v81hGWoNG5motq.e', 'egOmont1bHOyFw57PfiMfOaSswjKOhM81nLuKRMagfGOXTQKUO2WVLtxwcp7', '2017-05-21 16:42:15', '2017-05-23 10:26:08', 'Prezime', 'Adresa', '11000', 'B', 'Srbija', '011123456', 'admin', 1),
(6, 'Stasic', 'k@k.com2', '$2y$10$TEmXkdmqkcgEa1DMdW98L.cbGXO89FUcGET.M7EKjmemhKH4QDtfC', NULL, '2017-05-21 16:50:06', '2017-05-21 16:50:06', 's', 's', '1', 's', 'Srbija', '2', 'user', 1),
(8, 'Stasic', 'k@k.com4', '$2y$10$8ltjeOUZUp3cBVPcJ598ye6Dsk69DFG9lAfna60KhO6X54TIV3foK', 'kCtyiwlWmr9QcibLFQPCglfTI0E4Rh2meCre8d9sB8RfED21raIDQ3nbd19f', '2017-05-21 16:56:33', '2017-05-23 16:56:54', 'Guzic', 'd', '11000', 'c', 'Srbija', '011123456', 'user', 1),
(10, 'SVS', 'd@d.com1', '$2y$10$DDCN0Ci0pgo7xbjEPZOR.OkL0482j.0860E7uBVKB1PvNbXrZBDQO', NULL, '2017-05-22 15:51:58', '2017-05-27 09:39:41', 's', 's', '1', 's', 'Srbija', '1234', 'user', 0),
(12, 'Izmenjeni korisnik', 'd@d.com3', '$2y$10$4GE2HbZrAVIgF2VGeiaegewyR/N7fHFRPPujnD2PneAevq7EWkVu6', NULL, '2017-05-22 15:59:20', '2017-05-27 09:02:08', 's', 's', '1', 's', 'Srbija', '1', 'admin', 0),
(13, 's', 'd@d.com4', '$2y$10$dJXkPZwxD7/aJHoRQLGzYuHl.VVJdRbwSLEu7c6ZP6IwOlErxxLva', NULL, '2017-05-22 16:00:56', '2017-05-22 16:00:56', 's', 's', '1', 's', 'Srbija', '1', 'user', 1),
(14, 's', 'd@d.com7', '$2y$10$4.CjeAliPlpG0tYBtteaP.unY96opOcFpQ4Wo2DSk8uBWWzxElmQe', NULL, '2017-05-22 16:03:11', '2017-05-22 16:03:11', 's', 's', '1', 's', 'Srbija', '1', 'user', 1),
(16, 's', 'd@d.com9', '$2y$10$hH3UbP3YQSgAe2Abes837.Tovs4yv6SzKSbg3/tLcT6RDmzULoKeK', NULL, '2017-05-22 16:09:51', '2017-05-22 16:09:51', 's', 's', '1', 's', 'Srbija', '1', 'user', 1),
(17, 's', 'd@d.com10', '$2y$10$uJCbX1oKB/4Z93ilIEZJo.NxGligm3YL911rSbXvm1y68l/DjPsQK', NULL, '2017-05-22 16:13:25', '2017-05-22 16:13:25', 's', 's', '1', 's', 'Srbija', '1', 'user', 1),
(18, 's', 'd@d.com11', '$2y$10$NO.cPp/0FHkYPUDB5.P8feyDvFpMoApzzmG9a5w0vdVDGKHv88rca', NULL, '2017-05-22 16:14:43', '2017-05-22 16:14:43', 's', 's', '1', 's', 'Srbija', '1', 'user', 1),
(19, 's', 'd@d.com12', '$2y$10$fd05j6eGlams0JGqXyF2XeFWuKhzmEeTOEOlyBp0vgGb4heOVa7h2', NULL, '2017-05-22 16:17:46', '2017-05-22 16:17:46', 's', 's', '1', 's', 'Srbija', '1', 'user', 1),
(20, 's', 'd@d.com14', '$2y$10$oLSldtNkpxfRHaI3.eYI2eas6T0AK3.OFbvh.nHPu5.AmsSQASwxm', NULL, '2017-05-22 16:20:04', '2017-05-22 16:20:04', 's', 's', '1', 's', 'Srbija', '1', 'user', 1),
(21, 's', 'd@d.com15', '$2y$10$WpgeoebKfFCPyP91NTWzIebItdyZorlEOtlMUeznEseASZhPmwfK.', NULL, '2017-05-22 16:21:01', '2017-05-22 16:21:01', 's', 's', '1', 's', 'Srbija', '1', 'user', 1),
(22, 's', 'd@d.com16', '$2y$10$vik2QL8pXU220oES2NWuruvy0YEUDe49uSFHnt6VL0dDKT3VIVs6q', NULL, '2017-05-22 16:23:17', '2017-05-22 16:23:17', 's', 's', '1', 's', 'Srbija', '1', 'user', 1),
(23, 's', 'd@d.com17', '$2y$10$SFptKgUYFJ44ehzZJnYsd.4.qNgThH/P64/z40k1ikjajTr0pdaPG', NULL, '2017-05-22 16:28:33', '2017-05-22 16:28:33', 's', 's', '1', 's', 'Srbija', '1', 'user', 1),
(24, 's', 'd@d.com19', '$2y$10$bZDb51kWW48ivyHNIdiMH.3flOwdofwcCE3ll8kuuXbq3o23y5IgK', NULL, '2017-05-22 16:31:32', '2017-05-22 16:31:32', 's', 's', '1', 's', 'Srbija', '1', 'user', 1),
(25, 's', 'd@d.com20', '$2y$10$CBycWV.zDPQLahuU1In7SuSE/A3lbUThdzeRehBUbk.WwQLLMDFiq', NULL, '2017-05-22 16:33:07', '2017-05-22 16:33:07', 's', 's', '1', 's', 'Srbija', '1', 'user', 1),
(26, 's', 'd@d.com21', '$2y$10$2N6PvWSzJWeSUfIITH5IHupGXCyBpuPFw3kD2diiRKXjRMe7T2KOO', NULL, '2017-05-22 16:33:42', '2017-05-22 16:33:42', 's', 's', '1', 's', 'Srbija', '1', 'user', 1),
(27, 's', 'd@d.com22', '$2y$10$D4nN3.uJ6bXiu8I9pOB9W.Ybzj0tTe6mPCrZDbOmH46DKZ/yE.gvy', NULL, '2017-05-22 16:36:45', '2017-05-22 16:36:45', 's', 's', '1', 's', 'Srbija', '1', 'user', 1),
(28, 'a', 'a@a.com1', '$2y$10$rdm3NO/2Luoyw1frIGf8LONijyKQX8qbtlQT24Z3e/NJoCr6BSQdG', NULL, '2017-05-23 07:28:18', '2017-05-23 07:28:18', 'a', 'a', '1', 'a', 'Srbija', '1', 'user', 1),
(29, 'a', 'a@a.com3', '$2y$10$jwW15ZWyTHeDIypuJsLo2OHe5/SGJjSfu.oYkujPBNiKvLNVhZ3Zi', NULL, '2017-05-23 07:36:41', '2017-05-23 07:36:41', 'a', 'a', '1', 'a', 'Srbija', '1', 'user', 1),
(30, 'a', 'a@a.com4', '$2y$10$Pzi6kNJZKpaIxdKkyAcxIOkSRhK2O7tLS0iLP.r70OUFrAQqkIj0e', NULL, '2017-05-23 07:39:34', '2017-05-23 07:39:34', 'a', 'a', '1', 'a', 'Srbija', '1', 'user', 1),
(31, 'a', 'a@a.com5', '$2y$10$QJvRk5ne9GjwPa5isVDZSuYf3asKMI15D5qtCysKIqaTZ64U7lRiK', NULL, '2017-05-23 07:40:59', '2017-05-23 07:40:59', 'a', 'a', '1', 'a', 'Srbija', '1', 'user', 1),
(32, 'a', 'a@a.com6', '$2y$10$fosq6D3HJpVS85aNoOR0cuH5fqDC5nmtsP/mbgcuP.oAuJqayDyca', NULL, '2017-05-23 07:41:49', '2017-05-23 07:41:49', 'a', 'a', '1', 'a', 'Srbija', '1', 'user', 1),
(33, 'a', 'a@a.com7', '$2y$10$6LRI4s4V0e209qDM0j754u9vmlvmTu4MNC9CXAYy3rCSU9xqPCSsG', NULL, '2017-05-23 07:51:50', '2017-05-23 07:51:50', 'a', 'a', '1', 'a', 'Srbija', '1', 'user', 1),
(34, 'a', 'a@a.com8', '$2y$10$i3rLbhDwyJGaZ6VoiqKwwuGqZaNd6OIZduMbJUzTOrjVG2neyw5mm', NULL, '2017-05-23 07:52:46', '2017-05-23 07:52:46', 'a', 'a', '1', 'a', 'Srbija', '1', 'user', 1),
(35, 'a', 'a@a.com9', '$2y$10$uxvkB7feHfGDOug8kwTkAuhFQerv0JD26vLGASyqdW6jlWSL1py0y', NULL, '2017-05-23 07:53:46', '2017-05-23 07:53:46', 'a', 'a', '1', 'a', 'Srbija', '1', 'user', 1),
(36, 'a', 'a@a.com10', '$2y$10$eu44IPPcWsJeSWkzohLrvetNvGTg4HpniXk.yzrMk3KG4erO53K5y', NULL, '2017-05-23 07:56:12', '2017-05-23 07:56:12', 'a', 'a', '1', 'a', 'Srbija', '1', 'user', 1),
(37, 'a', 'a@a.com11', '$2y$10$5kJvbxiP80R2qek31HLzre5CnzIlFcnjuNoe7ncwoWRbxKIN0TYE.', NULL, '2017-05-23 07:57:02', '2017-05-23 07:57:02', 'a', 'a', '1', 'a', 'Srbija', '1', 'user', 1),
(38, 'a', 'a@a.com12', '$2y$10$u2yZn9TlvAudVoZ3T1QpfuMZ.JuJgqaXq2Lc8PoAC6AfB9ACO5wre', NULL, '2017-05-23 07:58:18', '2017-05-23 07:58:18', 'a', 'a', '1', 'a', 'Srbija', '1', 'user', 1),
(39, 'a', 'a@a.com14', '$2y$10$z4vwwL1s.LpzgxD0TF1n..k47iGyxhjyOO38mlvHZWmDGaCvy2nxe', NULL, '2017-05-23 08:00:07', '2017-05-23 08:00:07', 'a', 'a', '1', 'a', 'Srbija', '1', 'user', 1),
(40, 'a', 'a@a.com15', '$2y$10$XdoyU7L27KNeOlJJUmnju.MH2SzPdbolNk.sCsxzECmSWPIb0EOs6', NULL, '2017-05-23 08:18:16', '2017-05-23 08:18:16', 'a', 'a', '1', 'a', 'Srbija', '1', 'user', 1),
(41, 'a', 'a@a.com16', '$2y$10$NLHGyQ18H845FW0w.f4D/OBtsqXLZOqXQN/4bemJ097EP5uOcQln.', NULL, '2017-05-23 08:22:47', '2017-05-23 08:22:47', 'a', 'a', 'a', 'a', 'Srbija', '1', 'user', 1),
(42, 'a', 'a@a.com17', '$2y$10$FZGUTofiyG49Drg9fAIKRu4h.3cBm8KJ0ShIAszRjD/BCS/O2RZ.q', NULL, '2017-05-23 08:23:47', '2017-05-23 08:23:47', 'a', 'a', '1', 'a', 'Srbija', '1', 'user', 1),
(43, 'a', 'a@a.com18', '$2y$10$UlAtcOyIHIYPfWSNKPtTJ.QmM90GASGov7N8jvMEun3epiOopG7sK', NULL, '2017-05-23 08:25:02', '2017-05-23 08:25:02', 'a', 'a', '1', 'a', 'Srbija', '1', 'user', 1),
(44, 'a', 'a@a.com19', '$2y$10$Zrkya0eiRpIHxy7cGrSvTegLJL5NXwGhMzFdQPub1AcbA4K5kuDta', NULL, '2017-05-23 08:27:34', '2017-05-23 08:27:34', 'a', 'a', '1', 'a', 'Srbija', '1', 'user', 1),
(45, 'a', 'a@a.com20', '$2y$10$oyRZccUuCZFQNNDFCIaA7uikyNU3TEkVxNW9jJrL7c9NDF6UzfRFa', NULL, '2017-05-23 08:28:44', '2017-05-23 08:28:44', 'a', 'a', '1', 'a', 'Srbija', '1', 'user', 1),
(46, 'a', 'a@a.com21', '$2y$10$9BMjpDS3k9OTx5KKujLcveVITNuA9zLofIh4R3NeLPqfWejxP4umS', NULL, '2017-05-23 08:30:07', '2017-05-23 08:30:07', 'a', 'a', '1', 'a', 'Srbija', '1', 'user', 1),
(47, 'a', 'a@a.com22', '$2y$10$U3yRFqnrASqAnlYPiiPguO.puUfr7XZKX8Jr/XF.SXrszXKkXR9OK', NULL, '2017-05-23 08:31:19', '2017-05-23 17:46:39', 'a', 'a', '1', 'a', 'Srbija', '1', 'user', 0),
(48, 'a', 'a@a.com23', '$2y$10$90J1NWoPUKX6dYdW9sfWj.jQz5K0MhQQNYjgwjtwkLkNIMrTr/2hi', NULL, '2017-05-23 08:33:42', '2017-05-23 08:33:42', 'a', 'a', '1', 'a', 'Srbija', '1', 'user', 1),
(49, 'a', 'a@a.com24', '$2y$10$zWtWPJ.AcKL.6p0XZu99xeSy2Dd4kxvgYAMaNLOWMMkTNmCK1nhNy', NULL, '2017-05-23 08:34:36', '2017-05-23 08:34:36', 'a', 'a', '1', 'a', 'Srbija', '1', 'user', 1),
(50, 'a', 'a@a.com25', '$2y$10$JaQXnd7jqP/NG4VNPsccx.fPcPRVKspOvRwTjT8AvNMXSeyS9Jp7G', NULL, '2017-05-23 08:35:10', '2017-05-23 08:35:10', 'a', 'a', '1', 'a', 'Srbija', '1', 'user', 1),
(51, 'a', 'a@a.com26', '$2y$10$gb5M4hTMj3GmMNy.MPYz6OtD6NTbD/RmgNS5xTkaWvD7wHaVHpnJ.', NULL, '2017-05-23 08:38:06', '2017-05-23 08:38:06', 'a', 'a', '1', 'a', 'Srbija', '1', 'user', 1),
(52, 'a', 'a@a.com27', '$2y$10$IBuRI88RgQbGKqAdeinuNeeGvo9TPeTb4F17ph7MVrH.TVdge2aUO', NULL, '2017-05-23 08:39:09', '2017-05-23 08:39:09', 'a', 'a', '1', 'a', 'Srbija', '1', 'user', 1),
(53, 'a', 'a@a.com28', '$2y$10$W0KNt5seTcGgHMzX0rQqpex5VJTaOLCb453UFJShrP3t/Nyn3NoLa', NULL, '2017-05-23 08:40:16', '2017-05-23 17:46:37', 'a', 'a', '1', 'a', 'Srbija', '1', 'user', 0),
(54, 'a', 'a@a.com29', '$2y$10$nvgMzQdmfHTNJqHqbgQcLOOEko0FhFPMbs.8sa/m.GV3SuBVReCcu', NULL, '2017-05-23 08:42:28', '2017-05-23 08:42:28', 'a', 'a', '1', 'a', 'Srbija', '1', 'user', 1),
(55, 'a', 'a@a.com30', '$2y$10$2bC1aHLawF/6UOmUSn7KKO3kqcvItDgF19kLqcMUitxfncXFc3Rgy', NULL, '2017-05-23 08:48:44', '2017-05-23 08:48:44', 'a', 'a', '1', 'a', 'Srbija', '1', 'user', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
