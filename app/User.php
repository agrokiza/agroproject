<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\MyResetPassword;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;


class User extends Authenticatable implements AuditableContract, UserResolver
{
    use Auditable;
    use Notifiable;
    use SoftDeletes;

    protected $auditThreshold = 100;

    public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'subscription_id', 'name', 'email', 'password', 'surname', 'streetandnumber', 'postalcode', 'city', 'country', 'phonenumber', 'role', 'isActive', 'mqtt_super', 'pw', 'locale'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $hidden = [
        'password', 'remember_token',
    ];


    public function acl()
    {
        return $this->hasMany('App\Acl', 'email', 'email');
    }

    public function device()
    {
        return $this->hasMany('App\Device');
    }

    public function rule()
    {
        return $this->hasMany('App\Rule');
    }
    
    public function subscription(){
        return $this->belongsTo('App\Subscription');
    }

    public function getFullnameAttribute()
    {
        return $this->name . ' ' . $this->surname;
    }

    public function getFullnameEmailAttribute()
    {
        return $this->name . ' ' . $this->surname . ' - ' . $this->email;
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MyResetPassword($token));
    }

}
