<?php

namespace App;


use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;


class Subscription extends Model implements AuditableContract
{
    use Auditable;

    protected $table = 'subscriptions';

    protected $auditThreshold = 100;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'features'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $casts = ['features'=>'array'];

    public function user()
    {
        return $this->hasMany('App\User');
    }



}
