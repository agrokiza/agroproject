<?php

namespace App;


use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;


class Rule extends Model implements AuditableContract
{
    use Auditable;
    use SoftDeletes;

    protected $table = 'rules';

    protected $auditThreshold = 100;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'device_id', 'description', 'rule', 'is_active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $casts = [
        'rule' => 'json'
    ];


    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getStatusAttribute()
    {
        return Helpers\Helper::integerToYesNo($this->is_active);
    }

    protected $appends = ['status'];

}
