<?php

namespace App\Console;

use App\InputEvent;
use App\MqttMessage;
use App\SystemEvent;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\DB;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        $schedule->call(function () {
            $brisi = Carbon::now()->subDay(1);
            SystemEvent::where('ReceivedAt', '<', $brisi)->delete();

            //Mark events inactive
            $time_limit = Carbon::now()->subMinutes(config('inputevents.lifetime'));
            InputEvent::where('is_active', 1)->where('updated_at', '<', $time_limit)->update(['is_active' => 0]);

            //Copy values to history and delete messages older than 7 days

            //Copy output messages
            DB::transaction(function () {
                DB::statement('CREATE TEMPORARY TABLE temp_table (SELECT * FROM mqtt_messages WHERE type="o" AND id NOT IN (SELECT id FROM mqtt_messages_history WHERE type="o"));');
                DB::statement('INSERT INTO mqtt_messages_history (id, device_id, unit_id, type, message, enable, created_at) SELECT id, device_id, unit_id, type, message, enable, created_at FROM temp_table;');
                DB::statement('DROP TABLE temp_table;');
            });

            //Copy input messages (Min and Max for each hour)
            DB::transaction(function () {
                DB::statement('CREATE TEMPORARY TABLE temp_table (SELECT id, device_id, unit_id, type, MAX(CAST(message AS DECIMAL(5))) AS message, enable, created_at FROM `mqtt_messages` WHERE type="i" GROUP BY type, device_id, unit_id, DATE(created_at), HOUR(created_at));');
                DB::statement('INSERT INTO mqtt_messages_history (id, device_id, unit_id, type, message, enable, created_at) SELECT id, device_id, unit_id, type, message, enable, created_at FROM temp_table WHERE id NOT IN (SELECT id FROM mqtt_messages_history WHERE type="i");');
                DB::statement('DROP TABLE temp_table;');
            });

            DB::transaction(function () {
                DB::statement('CREATE TEMPORARY TABLE temp_table (SELECT id, device_id, unit_id, type, MIN(CAST(message AS DECIMAL(5))) AS message, enable, created_at FROM `mqtt_messages` WHERE type="i" GROUP BY type, device_id, unit_id, DATE(created_at), HOUR(created_at));');
                DB::statement('INSERT INTO mqtt_messages_history (id, device_id, unit_id, type, message, enable, created_at) SELECT id, device_id, unit_id, type, message, enable, created_at FROM temp_table WHERE id NOT IN (SELECT id FROM mqtt_messages_history WHERE type="i");');
                DB::statement('DROP TABLE temp_table;');
            });

            //Delete messages older than 7 days
            DB::transaction(function () {
                $messages_delete_time = Carbon::now()->subDays(7);
                DB::statement('DELETE FROM `mqtt_messages` WHERE created_at < "'.$messages_delete_time->toDateTimeString().'";');
            });

        })->hourly();

    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
