<?php

namespace App;


use App\Http\Controllers\DevicesController;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;


class Output extends Model implements AuditableContract
{
    use Auditable;
    use SoftDeletes;

    protected $table = 'outputs';

    protected $auditThreshold = 100;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'device_id', 'type_id', 'description', 'status', 'uui', 'ui_number', 'ui_show', 'ui_bg_color'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    public function device()
    {
        return $this->belongsTo('App\Device');
    }

    public function getOutputDataAttribute(){
        $device = new DevicesController();
        $outputs = $device->output_types;

        $output = $outputs->where('id',$this->type_id)->first();

        return $output;
    }

    public function getActiveRulesAttribute(){
        $rules = Rule::where('is_active',1)->where('rule','like','%"output_id":"'.$this->id.'"%')->count();

        if($rules>0){
            return 1;
        }
        else{
            return 0;
        }
    }

    protected $appends = ['activerules'];

}
