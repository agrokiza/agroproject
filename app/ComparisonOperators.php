<?php

namespace App;


class ComparisonOperators
{
    //
    public $operators = [];

    public function __construct()
    {
        $this->operators = [
            ['id' => '=', 'value' => '='],
            ['id' => '>', 'value' => '>'],
            ['id' => '>=', 'value' => '>='],
            ['id' => '<', 'value' => '<'],
            ['id' => '<=', 'value' => '<=']
        ];
    }
}
