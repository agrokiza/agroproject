<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use Illuminate\Support\Facades\Cookie;

class Locale
{
    protected $languages=['sr','de','en'];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Cookie::get('locale')===null){

            $def = $request->getPreferredLanguage($this->languages);
            $cookie=Cookie::forever('locale',$def);
            app()->setLocale($def);

            return $next($request)->withCookie($cookie);
        }
        else{
            
            app()->setLocale(Cookie::get('locale'));
            return $next($request);
        }

    }
}
