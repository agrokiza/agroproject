<?php

namespace App\Http\Controllers;

use App\InputEvent;
use App\Mail\EventNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Twilio\Rest\Client;

class NotificationController extends Controller
{
    //

    public function sendNotifications($event_id)
    {

        //Set event to notified when completed
        $event = InputEvent::find($event_id);

        //Set locale according to user locale
        app()->setLocale($event->input->device->user->locale);

        $event->is_notified = 1;
        $event->save();

        Mail::to($event->input->device->user->email)->send(new EventNotification($event));

        $sendSMS = false;

        $isActive = $subscription = $event->input->device->user->isActive;
        $subscription = $event->input->device->user->subscription;
        $subscription = $subscription["features"][0]['sms_notifications'];

        $mobile = $event->input->device->user->phonenumber;
        if ($subscription == true && $mobile != null && $mobile != '' && $isActive == 1) {
            $sendSMS = true;
        }

        if ($sendSMS == true) {
            //var_dump($sendSMS);
            // Your Account SID and Auth Token from twilio.com/console
            $sid = env('TWILIO_SID');
            $token = env('TWILIO_TOKEN');
            $client = new Client($sid, $token);

            $device = $event->input->device->description;
            $location = $event->input->device->location;
            $description = $event->input->description;
            $type = $event->type;
            $set = $event->set;
            $read = $event->read;

            //IMPORTANT!!!!!!!!! Keep it as short as possible
            //Replace special characters to save amount of data sent

            $find = array('Š', "š", "Ć", "ć", "Č", "č", "Ž", "ž", "Đ", "đ");
            $repplace = array('S', "s", "C", "c", "C", "c", "Z", "z", "Dj", "dj");

            $message = trans('messages.device') . ": " . $device . "\n" . trans('messages.location') . ": " .
                $location . "\n" . trans('messages.description') . ": " . $description . "\n" . trans('messages.type') . ": " . $type . "\n"
                . trans('messages.set') . ": " . $set . "\n" . trans('messages.read') . ": " . $read;

            $message = str_replace($find, $repplace, $message);

            // Use the client to do fun stuff like send text messages!
            $client->messages->create(
            // the number you'd like to send the message to
                $mobile,
                array(
                    // A Twilio phone number you purchased at twilio.com/console
                    //'from' => env('TWILIO_NUMBER'),
                    'from' => env('TWILIO_NAME'),
                    // the body of the text message you'd like to send

                    'body' => $message
                )
            );
        }
    }
}
