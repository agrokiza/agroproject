<?php

namespace App\Http\Controllers;

use App\ComparisonOperators;
use App\ConditionTypes;
use App\Device;
use App\Helpers\Helper;
use App\Input;
use App\LogicalOperators;
use App\Output;
use App\Rule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class AutomationController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public static function validator(array $data)
    {
        return Validator::make($data, [
            'user_id' => 'required|integer',
            'device_id' => 'required|integer',
            'description' => 'required|string',
            'conditions.*.logical_operator' => 'required|string',
            'conditions.*.condition_type' => 'required|integer',
            'conditions.*.input_id' => 'required|integer',
            'conditions.*.comparison_operator' => 'required|string',
            'conditions.*.input_value' => 'required|numeric',
            'conditions.*.threshold' => 'required|integer|min:0|max:10',
            'actions.*.action_type' => 'required|integer',
            'actions.*.output_id' => 'required|integer',
            'actions.*.output_value' => 'required|numeric',
        ]);
    }

    public function index()
    {
        $page_title = trans('messages.automation');
        return view('automation.index', compact('page_title'));
    }

    public function get($last = null)
    {
        if ($last == null) {
            $rules = Rule::where('user_id', Auth::id())->orderBy('id', 'desc')->get();
        } else {
            $rules = Rule::where('user_id', Auth::id())->orderBy('id', 'desc')->take($last)->get();
        }

        return compact('rules');
    }

    public function logicaloperatorsdropdown()
    {
        $operators = new LogicalOperators();
        $operators = $operators->operators;

        return array_pluck($operators, 'value', 'id');
    }

    public function conditiontypesdropdown()
    {
        $types = new ConditionTypes();
        $types = $types->types;

        return array_pluck($types, 'value', 'id');
    }

    public function inputsdropdown($device_id)
    {

        //Check does device belongs to user
        $device = Device::where('user_id', Auth::id())->where('id', $device_id)->first();

        $inputs = $device->input;

        $inputs = $inputs->pluck('description', 'id');

        return $inputs;
    }

    public function outputsdropdown($device_id)
    {

        //Check does device belongs to user
        $device = Device::where('user_id', Auth::id())->where('id', $device_id)->first();

        $outputs = $device->output;

        $outputs = $outputs->pluck('description', 'id');

        return $outputs;
    }

    public function comparisonoperatorsdropdown()
    {
        $operators = new ComparisonOperators();
        $operators = $operators->operators;

        return array_pluck($operators, 'value', 'id');
    }

    public function add()
    {
        $page_title = trans('messages.addAutomationRule');
        $devices = Device::where('user_id', Auth::id())->whereHas('input')->whereHas('output')->get();

        $devices = $devices->pluck('description', 'id');

        return view('automation.add', compact('page_title', 'devices'));
    }

    public function create(Request $request)
    {
        $postdata = $request->all();

        $postdata = Helper::checkboxHelper($postdata, ['is_active']);

        $postdata['user_id'] = Auth::id();

        $this->validator($postdata)->validate();

        //Check does all inputs and outputs belongs to device id

        try {
            $device_id = $postdata['device_id'];

            foreach ($postdata['conditions'] as $condition) {
                $input = Input::find($condition['input_id']);

                if ($input->device_id != $device_id) {
                    $poruka = trans('messages.databaseErrorOccured');
                    return Redirect::route('automation.all')->with('error', $poruka);
                }
            }

            foreach ($postdata['actions'] as $action) {
                $output = Output::find($action['output_id']);

                if ($output->device_id != $device_id) {
                    $poruka = trans('messages.databaseErrorOccured');
                    return Redirect::route('automation.all')->with('error', $poruka);
                }
            }

            //***************************************************************

            $rule = [
                'conditions' => $postdata['conditions'],
                'actions' => $postdata['actions']
            ];

            Rule::create([
                'user_id' => $postdata['user_id'],
                'device_id' => $device_id,
                'description' => $postdata['description'],
                'rule' => $rule,
                'is_active' => $postdata['is_active']
            ]);

            if ($postdata['is_active'] == 1) {
                MqttPublisherController::publishRules(Auth::id());
            }

            return Redirect::route('automation.all')->with('message', trans('messages.dataSaveSuccess'));
        } catch (\Exception $e) {
            $poruka = trans('messages.databaseErrorOccured');
            return Redirect::route('automation.all')->with('error', $poruka . ' ' . $e->getMessage());
        }
    }

    public function edit($id)
    {
        $page_title = trans('messages.editAutomationRule');

        $devices = Device::where('user_id', Auth::id())->whereHas('input')->whereHas('output')->get();

        $devices = $devices->pluck('description', 'id');

        $rule = Rule::find($id);

        $rules = $rule->rule;

        $conditions = $rules['conditions'];
        $actions = $rules['actions'];

        return view('automation.edit', compact('page_title', 'rule', 'actions', 'conditions', 'devices'));
    }

    public function update($id, Request $request)
    {
        $postdata = $request->all();

        $postdata = Helper::checkboxHelper($postdata, ['is_active']);

        $postdata['user_id'] = Auth::id();

        $this->validator($postdata)->validate();

        //Check does all inputs and outputs belongs to device id

        try {
            $device_id = $postdata['device_id'];

            foreach ($postdata['conditions'] as $condition) {
                $input = Input::find($condition['input_id']);

                if ($input->device_id != $device_id) {
                    $poruka = trans('messages.databaseErrorOccured');
                    return Redirect::route('automation.all')->with('error', $poruka);
                }
            }

            foreach ($postdata['actions'] as $action) {
                $output = Output::find($action['output_id']);

                if ($output->device_id != $device_id) {
                    $poruka = trans('messages.databaseErrorOccured');
                    return Redirect::route('automation.all')->with('error', $poruka);
                }
            }

            //***************************************************************

            $rule = [
                'conditions' => $postdata['conditions'],
                'actions' => $postdata['actions']
            ];

            $r = Rule::find($id);

            $r->update([
                'user_id' => $postdata['user_id'],
                'device_id' => $device_id,
                'description' => $postdata['description'],
                'rule' => $rule,
                'is_active' => $postdata['is_active']
            ]);

            if ($postdata['is_active'] == 1) {
                MqttPublisherController::publishRules(Auth::id());
            }

            return Redirect::route('automation.all')->with('message', trans('messages.dataSaveSuccess'));
        } catch (\Exception $e) {
            $poruka = trans('messages.databaseErrorOccured');
            return Redirect::route('automation.all')->with('error', $poruka . ' ' . $e->getMessage());
        }
    }

    public function delete($id)
    {
        $rule = Rule::find($id);

        $device_id = $rule->device_id;

        if ($rule->is_active == 1) {
            $send_settings = true;
        } else {
            $send_settings = false;
        }

        Rule::find($id)->delete();

        if ($send_settings == true) {
            MqttPublisherController::publishRules(Auth::id());
        }

        return response("OK", 200);
    }

    public function disableall($output_id)
    {

        $rules = Rule::where('is_active', 1)->where('rule', 'like', '%"output_id":"' . $output_id . '"%')->get();
        $outputs = [];
        foreach ($rules as $rule) {
            $r = $rule->rule;
            $actions = $r['actions'];
            foreach ($actions as $action) {
                $oid = $action['output_id'];
                array_push($outputs, $oid);

            }
        }

        //Remove duplicates
        $outputs = array_unique($outputs);

        //Set rules to inactive
        Rule::where('is_active', 1)->where('rule', 'like', '%"output_id":"' . $output_id . '"%')->update([
            'is_active' => 0
        ]);


        MqttPublisherController::publishRules(Auth::id());
        
        return $outputs;

    }
}
