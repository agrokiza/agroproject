<?php

namespace App\Http\Controllers;

use App\Device;
use App\Input;
use App\Output;
use App\Rule;
use Bluerhinos\phpMQTT;
use Illuminate\Http\Request;

class MqttPublishRulesController extends Controller
{
    //
    public static function publishRules(Request $request, $user_id, $check = true)
    {
        try {

            if ($check == true) {
                //Check if request is comming from servers IP address
                if (env('APP_ENV') != 'local') {
                    if ($request->getClientIp() != env('APP_HOST_IP')) {
                        return response(trans('messages.mqttNotAuthorizedUser'), 403);
                    }
                }
            }

            $devices = Device::where('user_id', $user_id)->get();

            $env = env('MQTT_ENV');
            $server = env('MQTT_HOST');     // change if necessary
            $port = env('MQTT_PORT');                     // change if necessary
            $username = env('MQTT_USERNAME');                   // set your username
            $password = env('MQTT_PASSWORD');                   // set your password
            $client_id = uniqid(); // make sure this is unique for connecting to server - you could use uniqid()

            $rules = Rule::where('user_id', $user_id)->where('is_active', 1)->get();

            $message = '{';

            $i = 0;
            foreach ($rules as $rule) {
                $r=$rule->rule;

                //Replace input ids with uui
                foreach ($r['conditions'] as $key=>$value){

                    $input = Input::find($value['input_id']);

                    $r['conditions'][$key]['input_id'] = $input->uui;

                }

                //Replace output ids with uui
                foreach ($r['actions'] as $key=>$value){

                    $output = Output::find($value['output_id']);

                    $r['actions'][$key]['output_id'] = $output->uui;

                }

                $message = $message . '"' . $rule->id . '":'.json_encode($r);

                $i++;
                if ($i < count($rules)) {
                    $message = $message . ',';
                }
            }

            $message = $message . '}';

            if ($env == 'prod') {
                //On server use localhost for sending (it is faster)
                $mqtt = new phpMQTT($server, $port, $client_id);
            } else {
                //On development use mqtt.stronganic.rs on port 8883
                $ca_file = env('MQTT_CA_FILE');
                $mqtt = new phpMQTT($server, $port, $client_id, $ca_file);
            }

            if ($mqtt->connect(true, NULL, $username, $password)) {

                //Publish new rules to all devices due to possible roaming rules
                foreach ($devices as $device) {
                    $topic = $device->udi . '/setrules';
                    //QOS 2 not supported yet
                    $mqtt->publish($topic, $message, 1);
                }

                $mqtt->close();
                return response('Published', 200);
            } else {
                return response(trans('messages.mqttTimeOut'), 403);
            }

        } catch (\Exception $ex) {
            return response($ex->getMessage(), 403);
        }
    }


}
