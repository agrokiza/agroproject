<?php

namespace App\Http\Controllers;

use App\Input;
use App\InputEvent;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CreateEventController extends Controller
{
    //
    public function createEvent(Request $request, $device_id, $uui, $message)
    {
        try {

            //Check if request is comming from servers IP address
            if (env('APP_ENV') != 'local') {
                if ($request->getClientIp() != env('APP_HOST_IP')) {
                    return response(trans('messages.mqttNotAuthorizedUser'), 403);
                }
            }
            $data = json_decode(str_replace("'", '"', $message));
            $data = $data->inputevent;

            $ev_type = $data->type;
            $ev_read = $data->read;
            $ev_set = $data->set;

            $input = Input::where('device_id', $device_id)->where('uui', $uui)->first();

            if ($input->device->user->isActive == true) {

                //Check is there existing events
                $time_limit = Carbon::now()->subMinutes(config('inputevents.lifetime'));

                $events = InputEvent::where('input_id', $input->id)->where('type', $ev_type)->where('is_active', 1)->where('updated_at', '>', $time_limit)->first();

                if (count($events) == 0) {
                    $event = InputEvent::create([
                        'input_id' => $input->id,
                        'type' => $ev_type,
                        'set' => $ev_set,
                        'read' => $ev_read,
                        'notify_me' => $input->notify_me
                    ]);

                } else {
                    $event = $events;
                    $event->update([
                        'read' => $ev_read
                    ]);
                }

                if ($event->notify_me == 1 && $event->is_notified == 0 && $event->is_active == 1) {
                    $notification = new NotificationController();
                    $notification->sendNotifications($event->id);
                }
                response('OK', 200);
            }

        } catch (\Exception $ex) {
            return response($ex->getMessage(), 403);
        }
    }
}
