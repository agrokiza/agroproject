<?php

namespace App\Http\Controllers;

use App\Device;
use App\Input;
use App\InputEvent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EventController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware(['auth', 'user']);
    }

    public function index(){
        $page_title = trans('messages.events');

        return view('events.index', compact('page_title'));
    }

    public function get($option){

        $devices = Device::where('user_id',Auth::id())->get(['id']);
        $dids = [];

        foreach ($devices as $device){
            array_push($dids,$device['id']);
        }

        $inputs=Input::whereIn('device_id',$dids)->get(['id']);

        $ids = [];

        foreach ($inputs as $input){
            array_push($ids,$input['id']);
        }

        $events = InputEvent::whereIn('input_id',$ids)->where(function ($query) use ($option){
            if($option==1 || $option==0){
                $query->where('is_active',$option);
            }
        })->orderBy('updated_at','desc')->get();

        return compact('events');

    }
}
