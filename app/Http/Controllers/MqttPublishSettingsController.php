<?php

namespace App\Http\Controllers;

use App\Device;
use Bluerhinos\phpMQTT;
use Illuminate\Http\Request;

class MqttPublishSettingsController extends Controller
{
    //
    public static function publishSettings(Request $request, $device_id, $check = true)
    {
        try {

            if ($check == true) {
                //Check if request is comming from servers IP address
                if (env('APP_ENV') != 'local') {
                    if ($request->getClientIp() != env('APP_HOST_IP')) {
                        return response(trans('messages.mqttNotAuthorizedUser'), 403);
                    }
                }
            }

            $device = Device::find($device_id);

            $user_id = $device->user_id;

            MqttPublishRulesController::publishRules($request, $user_id);

            $env = env('MQTT_ENV');
            $server = env('MQTT_HOST');     // change if necessary
            $port = env('MQTT_PORT');                     // change if necessary
            $username = env('MQTT_USERNAME');                   // set your username
            $password = env('MQTT_PASSWORD');                   // set your password
            $client_id = uniqid(); // make sure this is unique for connecting to sever - you could use uniqid()

            $topic = $device->udi . '/setsettings';

            $inputs = $device->input;
            //$inputs = $inputs->where('unit_id', '!=', 6);
            $message = '{';

            $i = 0;
            foreach ($inputs as $input) {
                $message = $message . '"' . $input->uui . '":{' . '"min":' . $input->min . ',"max":' . $input->max . '}';

                $i++;
                if ($i < count($inputs)) {
                    $message = $message . ',';
                }
            }

            $message = $message . '}';

            if ($env == 'prod') {
                //On server use localhost for sending (it is faster)
                $mqtt = new phpMQTT($server, $port, $client_id);
            } else {
                //On development use mqtt.stronganic.rs on port 8883
                $ca_file = env('MQTT_CA_FILE');
                $mqtt = new phpMQTT($server, $port, $client_id, $ca_file);

            }

            if ($mqtt->connect(true, NULL, $username, $password)) {
                //QOS 2 not supported yet
                $mqtt->publish($topic, $message, 1);
                $mqtt->close();
                return response('Published', 200);
            } else {
                return response(trans('messages.mqttTimeOut'), 403);
            }

        } catch (\Exception $ex) {
            return response($ex->getMessage(), 403);
        }
    }


}
