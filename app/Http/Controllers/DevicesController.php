<?php

namespace App\Http\Controllers;

use App\Acl;
use App\Device;
use App\Input;
use App\MqttMessage;
use App\MqttMessageHistory;
use App\Output;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class DevicesController extends Controller
{
    //
    public $output_types = [];

    public $input_units = [];

    //List of available box colors
    /*
    bg-light-blue
    bg-green
    bg-yellow
    bg-red
    bg-aqua

    bg-purple
    bg-blue
    bg-navy
    bg-teal
    bg-maroon
    bg-black
    bg-gray
    bg-olive
    bg-lime
    bg-orange
    bg-fuchsia
    */


    public function __construct()
    {
        $this->middleware(['auth', 'admin']);

        app()->setLocale(Cookie::get('locale'));

        //Prefix 1 for outputs
        $this->output_types = collect([
            ['id' => '1', 'name' => trans('messages.typePump'), 'class' => 'bg-teal'],
            ['id' => '2', 'name' => trans('messages.typeFan'), 'class' => 'bg-orange'],
            ['id' => '3', 'name' => trans('messages.typeAirRegulation'), 'class' => 'bg-light-blue'],
            ['id' => '4', 'name' => trans('messages.typeRelay'), 'class' => 'bg-light-maroon']
        ]);

        //Prefix 0 for inputs
        $this->input_units = collect([
            ['id' => '1', 'name' => trans('messages.unitAirTemperature'), 'class' => 'bg-light-blue'],
            ['id' => '2', 'name' => trans('messages.unitSoilTemperature'), 'class' => 'bg-olive'],
            ['id' => '3', 'name' => trans('messages.unitAirHumidity'), 'class' => 'bg-gray'],
            ['id' => '4', 'name' => trans('messages.unitSoilHumidity'), 'class' => 'bg-maroon'],
            ['id' => '5', 'name' => trans('messages.unitWaterTemperature'), 'class' => 'bg-blue'],
            ['id' => '6', 'name' => trans('messages.unitDigital'), 'class' => 'bg-green']
        ]);
    }

    public static function validator(array $data)
    {
        return Validator::make($data, [
            'user_id' => 'required|integer',
            'udi' => 'required|string|unique:devices',
        ]);
    }

    public static function validator_inputs(array $data, $device_id)
    {
        return Validator::make($data, [
            'uui' => 'string|unique:inputs,uui,null,id,device_id,' . $device_id
        ]);
    }

    public static function validator_outputs(array $data, $device_id)
    {
        return Validator::make($data, [
            'uui' => 'string|unique:outputs,uui,null,id,device_id,' . $device_id
        ]);
    }

    public function index()
    {
        $page_title = trans('messages.menuDevices');
        return view('devices.index', compact('page_title'));
    }

    public function get($last = null)
    {
        if ($last == null) {
            $devices = Device::orderBy('id', 'desc')->get();
        } else {
            $devices = Device::orderBy('id', 'desc')->take($last)->get();
        }

        return compact('devices');
    }

    public function add()
    {
        $page_title = trans('messages.addDevice');
        $users = User::all();
        $users = $users->pluck('fullnameemail', 'id');

        $ot = $this->output_types->pluck('name', 'id');
        $iu = $this->input_units->pluck('name', 'id');

        return view('devices.add', compact('users', 'ot', 'iu', 'page_title'));
    }

    public function create(Request $request)
    {
        try {
            $postdata = $request->all();

            $result = DB::transaction(function () use ($postdata, $request) {

                $udi = str_random(8) . '-' . str_random(8) . '-' . str_random(8) . '-' . str_random(8);

                $postdata['udi'] = $udi;

                $this->validator($postdata)->validate();

                $user = User::find($postdata['user_id']);

                $max_devices = $user->subscription->features;
                $max_devices = $max_devices[0]['max_devices'];

                $user_dev = count($user->device);

                //If there is more devices than allowed by subscription
                if ($user_dev >= $max_devices) {
                    $poruka = trans('messages.subscriptionMaxReached');
                    return ["udi" => $poruka, "success" => false];
                }


                $device = Device::create($postdata);

                //Add ACL entry

                Acl::create([
                    'email' => $device->user->email,
                    'topic' => $udi . '/#',
                    'rw' => 2
                ]);

                if (array_key_exists('inputs', $postdata)) {
                    $inputs = $postdata['inputs'];

                    foreach ($inputs as $input) {
                        $input['device_id'] = $device->id;
                        $input['uui'] = str_random(16);
                        $this->validator_inputs($input, $device->id);
                        Input::create($input);
                    }
                }

                if (array_key_exists('outputs', $postdata)) {
                    $outputs = $postdata['outputs'];

                    foreach ($outputs as $output) {
                        $output['device_id'] = $device->id;
                        $output['uui'] = str_random(16);
                        $this->validator_outputs($output, $device->id);
                        Output::create($output);
                    }
                }

                return ["udi" => $udi, "success" => true];
            });

            if ($result['success'] == true) {
                return Redirect::route('devices.all')->with('message', trans('messages.dataSaveSuccess') . ' UDI: ' . $result['udi']);
            } else {
                return Redirect::route('devices.all')->with('error', $result['udi']);
            }

        } catch (\Exception $e) {
            $poruka = trans('messages.databaseErrorOccured');
            return Redirect::route('devices.all')->with('error', $poruka . ' ' . $e->getMessage());
        }
    }

    public function delete($id)
    {
        Input::where('device_id', $id)->delete();
        Output::where('device_id', $id)->delete();
        $device = Device::find($id);
        //No need to delete this as it will be deleted automatically after 7 days
        //MqttMessage::where('topic', 'like', '%' . $device->udi . '%')->delete();

        MqttMessageHistory::where('device_id', $id)->delete();

        Acl::where('topic', 'like', '%' . $device->udi . '%')->delete();

        Device::where('id', $id)->delete();

        return response("OK", 200);
    }

    public function edit($id)
    {
        $page_title = trans('messages.editDevice');
        $users = User::all();
        $users = $users->pluck('fullnameemail', 'id');

        $ot = $this->output_types->pluck('name', 'id');
        $iu = $this->input_units->pluck('name', 'id');

        $device = Device::find($id);
        $inputs = $device->input;
        $outputs = $device->output;

        return view('devices.edit', compact('users', 'ot', 'iu', 'device', 'inputs', 'outputs', 'page_title'));
    }

    public function update($id, Request $request)
    {
        try {
            $postdata = $request->all();

            $device = Device::find($id);

            $result = DB::transaction(function () use ($postdata, $request, $device) {


                $postdata['udi'] = $device->udi;

                $device->update($postdata);

                $send_settings = false;

                if (array_key_exists('inputs', $postdata)) {
                    $inputs = $postdata['inputs'];
                    $iids = [];

                    foreach ($inputs as $input) {

                        if (array_key_exists('id', $input)) {
                            $i = Input::find($input['id']);

                            //Change this to true if there is difference
                            if ($i->min != $input['min'] || $i->max != $input['max']) {
                                $send_settings = true;
                            }

                            $i->update($input);
                        } else {
                            $input['device_id'] = $device->id;
                            $input['uui'] = str_random(16);
                            $this->validator_inputs($input, $device->id);
                            $i = Input::create($input);
                            $send_settings = true;
                        }
                        array_push($iids, $i->id);
                    }

                    //Obrisi visak
                    Input::where('device_id', $device->id)->whereNotIn('id', $iids)->delete();

                    if ($send_settings == true) {
                        MqttPublisherController::publishSettings($device->id);
                    }


                } else {
                    //Obrisi visak
                    Input::where('device_id', $device->id)->delete();
                }

                if (array_key_exists('outputs', $postdata)) {
                    $outputs = $postdata['outputs'];

                    $oids = [];

                    foreach ($outputs as $output) {

                        if (array_key_exists('id', $output)) {
                            $o = Output::find($output['id']);
                            $o->update($output);
                        } else {
                            $output['device_id'] = $device->id;
                            $output['uui'] = str_random(16);
                            $this->validator_outputs($output, $device->id);
                            $o = Output::create($output);
                        }
                        array_push($oids, $o->id);
                    }

                    //Obrisi visak
                    Output::where('device_id', $device->id)->whereNotIn('id', $oids)->delete();

                } else {
                    //Obrisi visak
                    Output::where('device_id', $device->id)->delete();
                }
            });

            return Redirect::route('devices.all')->with('message', trans('messages.dataSaveSuccess'));
        } catch (\Exception $e) {
            $poruka = trans('messages.databaseErrorOccured');
            return Redirect::route('devices.all')->with('error', $poruka . ' ' . $e->getMessage());
        }
    }

    public function setup($id)
    {
        $page_title = trans('messages.setupParameters');

        $device = Device::find($id);
        $inputs = $device->input;
        $outputs = $device->output;

        return view('devices.setup', compact('device', 'inputs', 'outputs', 'page_title'));
    }

}
