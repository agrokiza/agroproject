<?php

namespace App\Http\Controllers;

use App\SystemEvent;
use Illuminate\Http\Request;

class MqttLogsController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware(['auth','admin']);
    }

    public function index(){
        $page_title = trans('messages.menuMQTTLogs');
        return view('mqtt_logs.index', compact('page_title'));
    }

    public function get($last=null){
        if($last==null){
            $logs = SystemEvent::orderBy('id','desc')->get();
        }
        else{
            $logs = SystemEvent::orderBy('id','desc')->take($last)->get();
        }

        return compact('logs');
    }
}
