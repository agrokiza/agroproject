<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WelcomeController extends Controller
{
    //
    public function __construct(){
        $this->middleware('auth');
    }
    
    public function index(){
        if(Auth::user()->role=='user') {
            $page_title = trans('messages.controlPanel');
        }
        else
        {
            $page_title = '';
        }
        return view('welcome',compact('page_title'));
    }
}
