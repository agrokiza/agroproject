<?php

namespace App\Http\Controllers;

use App\Input;
use App\MqttMessage;
use App\MqttMessageHistory;
use App\Output;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HistoryController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function output($id){
        $out = Output::find($id);

        if($out->device->user->id!=Auth::id()){
            return response(trans('messages.notAuthorized'),403);
        }
        
        $history = MqttMessageHistory::where('device_id', '=', $out->device->id)->where('type', 'o')->where('unit_id', $id)->orderBy('created_at','desc')->take(50)->get();

        $history->makeHidden(['inputvalue','date'])->toArray();

        return compact('history');
        
    }

    public function digitalinput($id){
        $in = Input::find($id);

        if($in->device->user->id!=Auth::id()){
            return response(trans('messages.notAuthorized'),403);
        }

        $history = MqttMessageHistory::where('device_id', '=', $in->device->id)->where('type', 'i')->where('unit_id', $id)->orderBy('created_at','desc')->take(50)->get();

        $history->makeHidden(['inputvalue','date'])->toArray();

        return compact('history');

    }

    public function input($id){
        $in = Input::find($id);

        if($in->device->user->id!=Auth::id()){
            return response(trans('messages.notAuthorized'),403);
        }

        $month = Carbon::today()->month;
        $year = Carbon::today()->year;

        $stats = MqttMessageHistory::where('device_id', '=', $in->device->id)->where('type', 'i')->where('unit_id', $id)->whereMonth('created_at',$month)->whereYear('created_at',$year)->orderBy(DB::raw('DATE(created_at)'),'asc')->groupBy([DB::raw('DATE(created_at)')])->get([DB::raw('DATE(created_at) as created_at'),DB::raw('AVG(CAST(message AS DECIMAL(5))) as avg_val')]);

        $days = $stats;

        $days->makeHidden(['inputvalue','outputstate','datetime'])->toArray();

        $label = $in->description;

        return compact('label','days');

    }
}
