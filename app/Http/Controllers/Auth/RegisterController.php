<?php

namespace App\Http\Controllers\Auth;

use App\Acl;
use App\Helpers\Helper;
use App\Subscription;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Mockery\CountValidator\Exception;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    public $userRoles = [];

    public $userNoSuperRoles = [
        ['id' => 'user', 'value' => 'User'],
        ['id' => 'admin', 'value' => 'Admin'],
    ];

    public $locales = [
        ['id' => 'de', 'value' => 'de'],
        ['id' => 'en', 'value' => 'en'],
        ['id' => 'sr', 'value' => 'sr']
    ];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        app()->setLocale(Cookie::get('locale'));

        $this->userRoles = [
            ['id' => 'user', 'value' => trans('messages.user')],
            ['id' => 'admin', 'value' => trans('messages.admin')],
            //['id'=>'superadmin', 'value' => {{trans('messages.superadmin')}}],
        ];
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public static function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:191',
            'email' => 'required|string|email|max:191|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'surname' => 'required|string|max:191',
            'streetandnumber' => 'required|string|max:191',
            'postalcode' => 'required|string|max:191',
            'city' => 'required|string|max:191',
            'country' => 'required|string|max:191',
            'phonenumber' => 'required|str  ing|max:191',
            'role' => 'required|string|max:191',
            'subscription_id' => 'required|integer'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {
        try {

            if ($data['role'] != 'admin') {
                $data['mqtt_super'] = 0;
            }

            $user = User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
                'surname' => $data['surname'],
                'streetandnumber' => $data['streetandnumber'],
                'postalcode' => $data['postalcode'],
                'city' => $data['city'],
                'country' => $data['country'],
                'phonenumber' => $data['phonenumber'],
                'subscription_id' => $data['subscription_id'],
                'role' => $data['role'],
                'mqtt_super' => $data['mqtt_super'],
                'pw' => \App\Helpers\MqttPw::mqtt_pw($data['password']),
                'locale' => $data['locale']
            ]);

            Mail::send('emails.welcome', $data, function ($message) use ($data) {
                $message->from(trans('messages.emailAddress'), config('app.name'));
                $message->subject(trans('messages.welcome'));
                $message->to($data['email']);
            });

            //$this->reloadMQTTconfig();

            return [
                'user' => $user,
                'type' => 'message',
                'message' => trans('messages.newUserSuccess')
            ];

        } catch (\Exception $ex) {
            return [
                'user' => null,
                'type' => 'error',
                'message' => trans('messages.generalErrorMessage') . $ex->getMessage()
            ];
        }

    }

    public function reloadMQTTconfig()
    {
        $process = new Process('kill -HUP $(cat /var/run/mosquitto.pid)');
        $process->run();

        // executes after the command finishes
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
    }

    public function showRegistrationForm()
    {
        if (Auth::user()->role == "admin") {
            $page_title = trans('messages.registrujkorisnika');

            $roles = array_pluck($this->userRoles, 'value', 'id');

            $locales = array_pluck($this->locales, 'value', 'id');

            $subscriptions = Subscription::all();
            $subscriptions = $subscriptions->pluck('name', 'id');

            return view('auth.register', compact('page_title', 'roles', 'subscriptions', 'locales'));
        } else {
            Auth::logout();
            return redirect('/login');
        }
    }

    public function register(Request $request)
    {
        if (Auth::user()->role == "admin") {

            $this->validator($request->all())->validate();

            $data = $request->all();
            $data = Helper::checkboxHelper($data, ['mqtt_super']);

            $result = $this->create($data);

            return redirect(route('users.all'))->with($result['type'], $result['message']); // Change this route to your needs
        } else {
            Auth::logout();
            return redirect('/login');
        }
    }
}
