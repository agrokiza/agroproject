<?php

namespace App\Http\Controllers;

use App\Acl;
use App\Helpers\Helper;
use App\Http\Controllers\Auth\RegisterController;
use App\Subscription;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;


class KorisniciController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (Auth::user()->role == "admin") {
            $users = User::all();
            $page_title = trans('messages.users');

            return view("users.index", compact("users", "page_title"));
        } else {
            return redirect('/')->with('message', trans('messages.notAuthorized'));
        }
    }

    public function delete($id)
    {
        if (Auth::user()->role == "admin") {
            if (Auth::user()->id == $id) {
                return Redirect::route('users.all')->with('message', trans('messages.usersDeleteNotAllowed'));
            } else {
                $kor = User::find($id);
                $fullname = $kor->name . ' ' . $kor->surname;
                $kor->delete();

                return Redirect::route('users.all')->with('message', trans('messages.usersDeleteSuccesfull', ['name' => $fullname]));
            }
        } else {
            return redirect('/')->with('message', trans('messages.notAuthorized'));
        }
    }

    public function edit($id)
    {
        $rs = new RegisterController();

        if (Auth::user()->role == "admin") {
            if (Auth::user()->id == $id) {
                return Redirect::route('users.all')->with('message', trans('messages.usersChangeNotAllowed'));
                // Change this route to your needs
            } else {
                $user = User::find($id);
                $page_title = trans('messages.userChange');

                $roles = array_pluck($rs->userRoles, 'value', 'id');

                $locales = array_pluck($rs->locales, 'value', 'id');

                $subscriptions = Subscription::all();
                $subscriptions = $subscriptions->pluck('name', 'id');

                return view('users.edit', compact('user', 'page_title', 'roles', 'subscriptions', 'locales'));
            }
        } else {
            return redirect('/')->with('message', trans('messages.notAuthorized'));
        }

    }

    public function activatedeactivate($id)
    {
        if (Auth::user()->role == "admin") {
            if (Auth::user()->id == $id) {
                return Redirect::route('users.all')->with('message', trans('messages.usersChangeNotAllowed'));
            } else {
                $data = array_except(Input::all(), '_method');

                $kor = User::find($id);

                if ($kor->isActive == 1)
                    $kor->update(['isActive' => 0]);
                else
                    $kor->update(['isActive' => 1]);

                $fullname = $kor->name . ' ' . $kor->surname;

                return Redirect::route('users.all')->with('message', trans('messages.userDataChanged', ['name' => $fullname]));
            }
        } else {
            return redirect('/')->with('message', trans('messages.notAuthorized'));
        }

    }

    public static function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:191',
            'surname' => 'required|string|max:191',
            'streetandnumber' => 'required|string|max:191',
            'postalcode' => 'required|string|max:191',
            'city' => 'required|string|max:191',
            'country' => 'required|string|max:191',
            'phonenumber' => 'required|string|max:191',
            'role' => 'required|string|max:191',
            'subscription_id' => 'required|integer'
        ]);
    }

    public function update($id, Request $request)
    {
        if (Auth::user()->role == "admin") {
            if (Auth::user()->id == $id) {
                return Redirect::route('users.all')->with('message', trans('messages.usersChangeNotAllowed'));
                // Change this route to your needs
            } else {
                $data = $request->all();

                $data = Helper::checkboxHelper($data, ['mqtt_super']);

                $kor = User::find($id);

                $this->validator($data)->validate();

                if ($data['role'] != 'admin') {
                    $data['mqtt_super'] = 0;
                }

                $kor->update([
                    'name' => $data['name'],
                    'surname' => $data['surname'],
                    'streetandnumber' => $data['streetandnumber'],
                    'postalcode' => $data['postalcode'],
                    'city' => $data['city'],
                    'country' => $data['country'],
                    'phonenumber' => $data['phonenumber'],
                    'role' => $data['role'],
                    'mqtt_super' => $data['mqtt_super'],
                    'subscription_id' => $data['subscription_id'],
                    'locale' => $data['locale']
                ]);

                $fullname = $kor->name . ' ' . $kor->surname;

                return Redirect::route('users.all')->with('message', trans('messages.userDataChanged', ['name' => $fullname]));
            }
        } else {
            return redirect('/')->with('message', trans('messages.notAuthorized'));
        }

    }

    // ------------------- za menjanje passworda -------------------
    public function credentials()
    {
        $user = User::find(Auth::User()->id);
        $page_title = trans('messages.profile');
        return view('users.changepassword', compact('user', 'page_title'));
    }

    public function admin_credential_rules(array $data)
    {
        $messages = [
            'current-password.required' => trans('messages.pleaseEnterCurrentPassword'),
            'password.required' => trans('messages.pleaseEnterNewPassword'),
        ];

        $validator = Validator::make($data, [
            'oldpassword' => 'required',
            'password' => 'required|same:password',
            'password_confirmation' => 'required|same:password',
        ], $messages);

        return $validator;
    }

    public function postCredentials(Request $request)
    {

        if (Auth::Check()) {
            $request_data = $request->All();

            $this->admin_credential_rules($request_data)->validate();

            $current_password = Auth::User()->password;
            if (Hash::check($request_data['oldpassword'], $current_password)) {
                $user_id = Auth::User()->id;
                $obj_user = User::find($user_id);
                $obj_user->password = Hash::make($request_data['password']);;
                $obj_user->save();
                return redirect('/')->with('message', trans('messages.passwordChanged'));
            } else {
                return redirect(route('users.credentials.changepassword'))->with('message', trans('messages.pleaseEnterCorrectPassword'));
            }

        } else {
            return redirect()->to('/');
        }
    }
    // ------------------- za menjanje passworda -------------------
}
