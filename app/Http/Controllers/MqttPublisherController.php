<?php

namespace App\Http\Controllers;

use App\Device;
use App\Output;
use Bluerhinos\phpMQTT;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MqttPublisherController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public static function publish($device_id, $output_id = null, $message)
    {
        try {

            $device = Device::find($device_id);

            //Check if device belong to user
            if (Auth::user()->role == "user") {
                if ($device->user->id != Auth::user()->id) {
                    return response(trans('messages.mqttNotAuthorizedUser'), 403);
                }
            }

            $env = env('MQTT_ENV');
            $server = env('MQTT_HOST');     // change if necessary
            $port = env('MQTT_PORT');                     // change if necessary
            $username = env('MQTT_USERNAME');                   // set your username
            $password = env('MQTT_PASSWORD');                   // set your password
            $client_id = uniqid(); // make sure this is unique for connecting to sever - you could use uniqid()

            if ($output_id == null || $output_id == "null") {
                if ($message == "ping") {
                    $topic = $device->udi . '/question';
                    $message = '?';
                } elseif ($message == "clock") {
                    $topic = $device->udi . '/clock';
                    $message = '?';
                }
            } else {
                $output = Output::find($output_id);
                $topic = $device->udi . '/' . $output->uui;
            }

            if ($env == 'prod') {
                //On server use localhost for sending (it is faster)
                $mqtt = new phpMQTT($server, $port, $client_id);
            } else {
                //On development use mqtt.stronganic.rs on port 8883
                $ca_file = env('MQTT_CA_FILE');
                $mqtt = new phpMQTT($server, $port, $client_id, $ca_file);

            }

            if ($mqtt->connect(true, NULL, $username, $password)) {
                //QOS 2 not supported yet
                $mqtt->publish($topic, $message, 1);
                $mqtt->close();
                return response('Published', 200);
            } else {
                return response(trans('messages.mqttTimeOut'), 403);
            }

        } catch (\Exception $ex) {
            return response($ex->getMessage(), 403);
        }
    }

    public static function publishSettings($id)
    {
        $request = new Request();

        MqttPublishSettingsController::publishSettings($request, $id, false);
    }

    public static function publishRules($user_id)
    {
        $request = new Request();

        MqttPublishRulesController::publishRules($request, $user_id, false);
        
    }
}
