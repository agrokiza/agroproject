<?php

namespace App\Http\Controllers;

use App\Device;
use App\Helpers\Helper;
use App\Input;
use App\Output;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class DevicesCustomizeController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware(['auth', 'user']);
    }

    public function customize($id)
    {

        $device = Device::findOrFail($id);

        if ($device->user->id != Auth::id()) {
            return redirect('/')->with('error', trans('messages.notAuthorized'));
        }

        $page_title = trans('messages.customizeDevice');

        $inputs = $device->input;
        $outputs = $device->output;

        return view('devices.customize.customize', compact('device', 'inputs', 'outputs', 'page_title'));
    }

    public function customizePost(Request $request, $id)
    {
        try {
            $postdata = $request->all();

            $device = Device::find($id);

            if ($device->user->id != Auth::id()) {
                return redirect('/')->with('error', trans('messages.notAuthorized'));
            }

            $result = DB::transaction(function () use ($postdata, $request, $device) {

                $device->update([
                    "description" => $postdata["description"],
                    "location" => $postdata["location"]
                ]);

                $send_settings = false;

                if (array_key_exists('inputs', $postdata)) {
                    $inputs = $postdata['inputs'];

                    foreach ($inputs as $input) {

                        if (array_key_exists('id', $input)) {
                            $i = Input::find($input['id']);

                            //Protection level
                            if ($i->device->id != $device->id) {
                                return redirect('/')->with('error', trans('messages.notAuthorized'));
                            }
                            $input = Helper::checkboxHelper($input, ['notify_me']);

                            //Change this to true if there is difference
                            if ($i->min != $input['min'] || $i->max != $input['max']) {
                                $send_settings = true;
                            }

                            if(array_key_exists('ui_ind_color',$input)){
                                $ind_color = $input["ui_ind_color"];
                            }
                            else{
                                $ind_color = null;
                            }
                            
                            $i->update([
                                "description" => $input["description"],
                                "min" => $input["min"],
                                "max" => $input["max"],
                                "ui_bg_color" => $input["ui_bg_color"],
                                "ui_ind_color" => $ind_color,
                                "notify_me" => $input["notify_me"]
                            ]);
                        }
                    }

                }

                if($send_settings==true){
                    //Temporary commented out as device is picking up new settings each time user main page is refreshed
                    //MqttPublisherController::publishSettings($device->id);
                }

                if (array_key_exists('outputs', $postdata)) {
                    $outputs = $postdata['outputs'];

                    foreach ($outputs as $output) {

                        if (array_key_exists('id', $output)) {
                            $o = Output::find($output['id']);

                            //Protection level
                            if ($o->device->id != $device->id) {
                                return redirect('/')->with('error', trans('messages.notAuthorized'));
                            }

                            $o->update([
                                "description" => $output["description"],
                                "ui_bg_color" => $output["ui_bg_color"]
                            ]);
                        }
                    }

                }
            });

            return Redirect::route('home')->with('message', trans('messages.dataSaveSuccess'));
        } catch (\Exception $e) {
            $poruka = trans('messages.databaseErrorOccured');
            return Redirect::route('home')->with('error', $poruka . ' ' . $e->getMessage());
        }
    }

    public function showhide($type, $id, $state)
    {
        if ($state == 'true') {
            $state = 1;
        } else {
            $state = 0;
        }

        switch ($type) {
            case 'device':
                $device = Device::find($id);

                if ($device->user->id != Auth::id()) {
                    return response(trans('messages.notAuthorized'), 403);
                }

                $device->update([
                    'ui_show' => $state
                ]);

                response('OK', 200);
                break;

            case 'input':
                $i = Input::find($id);

                if ($i->device->user->id != Auth::id()) {
                    return response(trans('messages.notAuthorized'), 403);
                }

                $i->update([
                    'ui_show' => $state
                ]);

                response('OK', 200);
                break;

            case 'output':
                $o = Output::find($id);

                if ($o->device->user->id != Auth::id()) {
                    return response(trans('messages.notAuthorized'), 403);
                }

                $o->update([
                    'ui_show' => $state
                ]);

                response('OK', 200);
                break;

            default:
                break;
        }
    }
}
