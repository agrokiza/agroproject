<?php

namespace App;


class ConditionTypes
{
    //
    public $types = [];

    public function __construct()
    {
        $this->types = [
            ['id' => '1', 'value' => trans('messages.conditionTypeNormal')]
        ];
    }
}
