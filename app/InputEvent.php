<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InputEvent extends Model
{
    //
    use SoftDeletes;

    protected $table = "input_events";

    protected $fillable = ['input_id', 'type', 'set', 'read', 'notify_me', 'is_notified', 'is_active'];

    protected $dates = ['deleted_at'];

    public function input()
    {
        return $this->belongsTo('App\Input');
    }

    public function getDeviceDescriptionAttribute()
    {
        return $this->input->device->description;
    }

    public function getDeviceLocationAttribute()
    {
        return $this->input->device->location;
    }

    public function getInputDescriptionAttribute()
    {
        return $this->input->description;
    }

    protected $appends = ['devicedescription', 'devicelocation', 'inputdescription'];

    protected $hidden = ['input'];
}
