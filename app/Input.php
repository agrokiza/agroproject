<?php

namespace App;


use App\Http\Controllers\DevicesController;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;


class Input extends Model implements AuditableContract
{
    use Auditable;
    use SoftDeletes;

    protected $table = 'inputs';

    protected $auditThreshold = 100;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'device_id', 'unit_id', 'description', 'min', 'max', 'uui', 'ui_number', 'ui_show', 'ui_bg_color', 'ui_ind_color', 'notify_me'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    public function device()
    {
        return $this->belongsTo('App\Device');
    }

    public function event()
    {
        return $this->hasMany('App\InputEvent');
    }

    public function getInputDataAttribute()
    {
        $device = new DevicesController();
        $inputs = $device->input_units;

        $input = $inputs->where('id', $this->unit_id)->first();

        return $input;
    }

    protected $hidden = ['created_at', 'updated_at', 'deleted_at', 'uui', 'ui_number', 'ui_show', 'ui_bg_color', 'device_id', 'unit_id'];

}
