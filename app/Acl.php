<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Acl extends Model implements AuditableContract
{
    use Auditable;

    protected $auditThreshold = 100;

    protected $table = "acls";

    protected $fillable = [
         'email', 'topic', 'rw'
    ];

    public function user(){
        return $this->belongsTo('App\User','email','email');
    }

    //
}
