<?php

namespace App\Mail;

use App\InputEvent;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EventNotification extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     *
     */
    public $event;

    public function __construct(InputEvent $event)
    {
        //
        $this->event = $event;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.eventnotification')->subject(trans('messages.eventNotification').": ".$this->event->input->description.' ('.$this->event->type.')');
    }
}
