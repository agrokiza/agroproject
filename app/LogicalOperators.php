<?php

namespace App;


class LogicalOperators
{
    //
    public $operators = [];

    public function __construct()
    {
        $this->operators = [
            ['id' => 'AND', 'value' => trans('messages.and')],
            ['id' => 'OR', 'value' => trans('messages.or')]
        ];
    }
}
