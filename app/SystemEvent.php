<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SystemEvent extends Model
{
    //
    protected $connection = "mysql1";
    protected $table = "SystemEvents";

    protected $fillable = ['Message', 'ReceivedAt'];

    protected $dates = [
        'ReceivedAt'
    ];


}
