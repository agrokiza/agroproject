<?php

namespace App;


use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;


class Device extends Model implements AuditableContract
{
    use Auditable;
    use SoftDeletes;

    protected $table = 'devices';

    protected $auditThreshold = 100;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'udi', 'description', 'mac', 'ip', 'location', 'ip', 'last_activity', 'is_active', 'ui_show', 'ui_number'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function input()
    {
        return $this->hasMany('App\Input');
    }

    public function output()
    {
        return $this->hasMany('App\Output');
    }

    public function getUserFullNameAttribute(){
        return $this->user->fullname;
    }

    public function getStatusAttribute(){
        return Helpers\Helper::integerToYesNo($this->is_active);
    }

    protected $appends = ['userfullname','status'];

    protected $hidden = ['user'];
}
