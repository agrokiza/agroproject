<?php

namespace App\Helpers;

class Helper
{

    // poziva se sa {{ Helper::role(Auth::user()->role) }}
    public static function roleToString($role)
    {
        if ($role == 'admin') {
            return trans('messages.admin');
        } else {
            return trans('messages.user');
        }
    }

    //This function is used to insert missing index key when checkboxes are used
    public static function checkboxHelper($request_array, $keys)
    {
        foreach ($keys as $key) {
            if (!array_key_exists($key, $request_array)) {
                $request_array[$key] = 0;
            } else {
                $request_array[$key] = 1;
            }
        }
        return $request_array;
    }

    public static function integerToYesNo($int)
    {
        if($int==1){
            return trans('messages.yes');
        }
        else{
            return trans('messages.no');
        }

    }
}

?>