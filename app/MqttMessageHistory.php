<?php

namespace App;


use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;


class MqttMessageHistory extends Model
{

    protected $table = 'mqtt_messages_history';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'device_id', 'unit_id', 'type', 'message', 'enable', 'created_at'
    ];

    protected $dates = ['created_at'];

    protected $hidden = ['id', 'device_id', 'unit_id', 'type', 'message', 'enable', 'created_at'];

    public function getOutputStateAttribute()
    {
        if ($this->message == 'true') {
            return trans('messages.toggleOn');
        } else {
            return trans('messages.toggleOff');
        }
    }

    public function getInputValueAttribute()
    {
        return floatval($this->message);
    }

    public function getDateTimeAttribute()
    {
        return date('d.m.Y H:i:s', strtotime($this->created_at));
    }

    public function getDateAttribute()
    {
        return date('d.m.Y', strtotime($this->created_at));
    }

    protected $appends = ['outputstate', 'inputvalue', 'date', 'datetime'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
}
