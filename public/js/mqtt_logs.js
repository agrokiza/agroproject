
var table;
$(function () {
    table = $("#logs_table").DataTable(
        {
            ajax: {
                url: "../../../mqttlogs/get/1000",
                dataSrc: 'logs'
            },

            "processing": true,

            "columnDefs": [
                {
                    "targets": 0,
                    "data": 'Message',
                    "defaultContent": ""
                },
                {
                    "targets": 1,
                    "data": 'ReceivedAt',
                    "defaultContent": ""
                },

            ],

            //Cuvanje podesavanja tabele
            "stateSave": true,
            "stateSaveParams": function (settings, data) {
                data.search.search = "";
                //console.log(data);
            }

            ,
            //Iskljuci inicijalno sortiranje
            "language": {
                // TODO: Promeniti jezik!
                "url": jezik,
                "autoWidth": true
            }
        }
    )
    ;
});

function osveziTabelu(){
    var last = $('#last').val();
    table.ajax.url('../../../mqttlogs/get/'+last).load();
}
