/**
 * Created by belte on 29.10.2017..
 */
$(function(){
    hideMqttSuper();
});

function hideMqttSuper(){
    var role = $('#role').val();
    var mqtt = $('#mqtt_super');

    if(role=='admin'){
        mqtt.show();
    }
    else{
        mqtt.hide();
        $('[name=mqtt_super]').removeAttr('checked');
    }
}