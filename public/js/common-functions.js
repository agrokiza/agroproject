/**
 * Created by belte on 25.11.2017..
 */
function populateDropdown(value, fieldID, url, isChosen=true, triggerChange=false) {
    var combo = $('#' + fieldID);
    $.getJSON(url, function (json) {
        $.each(json, function (index, object) {
            var selected = '';
            if (index == value) {
                selected = ' selected';
            }
            combo.append('<option value="' + index + '"' + selected + '>' + object + '</option>');
        });

        if (isChosen == true) {
            combo.chosen({
                width: '100%',
                no_results_text: Lang.get('messages.noResultsText'),
                placeholder_text_multiple: Lang.get('messages.placeholderTextMultiple'),
                placeholder_text_single: Lang.get('messages.placeholderTextSingle')
            });
        }
        
        if(triggerChange==true){
            combo.trigger('change');
        }

    }).fail(function () {
        console.log('Populate ' + fieldID + ' failed.');
    });
}

function RandomID() {
    var rString = randomString(40, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
    return rString;
}

function randomString(length, chars) {
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
}
