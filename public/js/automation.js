var counter = 1;
var addLogicalOperator = false;
var prevDevice = '';

$(function () {

    //Enable validation
    $.validator.setDefaults({ignore: ":hidden:not(select)"}); //This must be set for all select using Chosen

    var valid = $('#myForm').validate({
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help-block");
            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.parent("label"));
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
        }
    });

    var deviceCombo = $('[name=device_id]');
    deviceCombo.on('change', function () {
        if (prevDevice != '' && $('[name^=conditions]').length > 0) {
            bootbox.confirm({
                message: "<div class='row center-row-my'>" +
                "<div class='col-sm-2 center-my'" +
                "<span style='font-size: 18px; font-weight: bold; color: black;'>" +
                "<i class='fa fa-question fa-3x' " +
                "aria-hidden='true' style='float: left;'></i></span></div> " +
                "<div class='col-sm-10 center-my'" +
                "<span style='font-size: 18px; font-weight: bold; color: black;'>"+ Lang.get('messages.automationRuleWillBeLost') +"</span></div> " +
                "</div>",
                size: 'normal',
                className: '',
                buttons: {
                    confirm: {
                        label: Lang.get('messages.yes'),
                        className: 'btn-danger'
                    },
                    cancel: {
                        label: Lang.get('messages.no'),
                        className: 'btn-success'
                    }
                },
                callback: function (result) {
                    if (result == true) {
                        $('#containerConditions').empty();
                        if ($('[name=device_id]').val() != '') {
                            $('#containerConditionsMaster').fadeIn();
                        }
                        else {
                            $('#containerConditionsMaster').fadeOut();
                        }
                        $('#containerActionsMaster').fadeOut();
                        $('#containerActions').empty();
                    }
                    else {
                        $('[name=device_id]').val(prevDevice);
                    }
                }
            });
        }
        else {
            if (this.value != '') {
                $('#containerConditionsMaster').fadeIn();
            }
            else {
                $('#containerConditionsMaster').fadeOut();
                $('#containerConditions').empty();
            }
        }
    });

    deviceCombo.on('focus', function () {
        prevDevice = this.value;
    });

    deviceCombo.trigger('change');
});

function AddCondition(values) {

    // Default options
    var vals = $.extend({
        logical_operator: '',
        condition_type: '',
        input_id: '',
        comparison_operator: '',
        input_value: '',
        threshold: 1
    }, values);

    var div = document.createElement('DIV');
    div.id = "ConditionDiv" + counter;
    div.className = "col-lg-12";
    var logOpRow = '';


    logOpRow = '<div id="conditions_log_operator_container-' + counter + '" class="col-lg-2">' +
        '<label><small>' + Lang.get('messages.logicalOperator') + '</small></label>' +
        '<select class="form-control" id="conditions_log_operator-' + counter + '" name="conditions[' + counter + '][logical_operator]" required></select>' +
        '</div>';


    var html = '<div class="row" style="padding-top: 15px;">' +
        logOpRow +
        '<div class="col-lg-2">' +
        '<label><small>' + Lang.get('messages.conditionType') + '</small></label>' +
        '<select class="form-control" id="conditions_condition_type-' + counter + '" name="conditions[' + counter + '][condition_type]" required>' +
        '</select>' +
        '</div> ' +

        '<div class="col-lg-3">' +
        '<label><small>' + Lang.get('messages.input') + '</small></label>' +
        '<select class="form-control" id="conditions_input_id-' + counter + '" name="conditions[' + counter + '][input_id]" required>' +
        '<option value=""></option>' +
        '</select>' +
        '</div>' +

        '<div class="col-lg-1">' +
        '<label><small>' + Lang.get('messages.comparisonOperator') + '</small></label>' +
        '<select class="form-control" id="conditions_comparison_operator-' + counter + '" name="conditions[' + counter + '][comparison_operator]" required>' +
        '</select>' +
        '</div>' +

        '<div class="col-lg-1">' +
        '<label><small>' + Lang.get('messages.inputValue') + '</small></label>' +
        '<input type="number" step="0.01" class="form-control" id="conditions_input_value-' + counter + '" name="conditions[' + counter + '][input_value]" value="' + vals.input_value + '" required>' +
        '</div>' +

        '<div class="col-lg-1">' +
        '<label><small>' + Lang.get('messages.threshold') + '</small></label>' +
        '<input type="number" step="1" min="0" max="10" class="form-control" id="conditions_threshold-' + counter + '" name="conditions[' + counter + '][threshold]" value="' + vals.threshold + '" required>' +
        '</div>' +

        '<div class="col-lg-1" style="padding-top: 24px;">' +
        '<button id="buttonConditionRemove' + counter + '" class="btn btn-box btn-danger" type="button" onclick = "RemoveCondition(this)">' +
        '<i class="fa fa-times"></i>' +
        '</button>' +

        '</div>' +

        '</div>' +
        '<hr>';

    div.innerHTML = html;

    document.getElementById("containerConditions").appendChild(div);


    populateDropdown(vals.condition_type, 'conditions_condition_type-' + counter, '/automation/conditiontypesdropdown', false, false);
    populateDropdown(vals.input_id, 'conditions_input_id-' + counter, '/automation/inputsdropdown/' + $('[name=device_id]').val(), false, false);
    populateDropdown(vals.comparison_operator, 'conditions_comparison_operator-' + counter, '/automation/comparisonoperatorsdropdown', false, false);

    if (addLogicalOperator == true) {
        populateDropdown(vals.logical_operator, 'conditions_log_operator-' + counter, '/automation/logicaloperatorsdropdown', false, false);
    }
    else {
        populateDropdown(vals.logical_operator, 'conditions_log_operator-' + counter, '/automation/logicaloperatorsdropdown', false, false);
        $('#conditions_log_operator_container-' + counter).hide();
        var cloCombo = $('#conditions_log_operator-' + counter);
        cloCombo.val("AND").change();
        cloCombo.removeAttr('required');
    }

    $('#containerActionsMaster').fadeIn();

    counter++;
    addLogicalOperator = true;
}

function RemoveCondition(div) {

    document.getElementById("containerConditions").removeChild(div.parentNode.parentNode.parentNode);

    var numOfConditions = $('[name^=conditions]').length;

    if (numOfConditions == 0) {
        addLogicalOperator = false;
    }

    //Hide logical operator on first condition
    var firstCondition = $('[id^=conditions_log_operator-]');

    $(firstCondition[0]).val("AND").change();
    $(firstCondition[0]).parent('div').hide();
    $(firstCondition[0]).removeAttr('required');
}


function AddAction(values) {

    // Default options
    var vals = $.extend({
        action_type: '1',
        output_id: '',
        output_value: ''
    }, values);

    var div = document.createElement('DIV');
    div.id = "ActionDiv" + counter;
    div.className = "col-lg-12";


    var html = '<div class="row" style="padding-top: 15px;">' +
        '<input type="hidden" name="actions[' + counter + '][action_type]" value="' + vals.action_type + '">' +

        '<div class="col-lg-3">' +
        '<label><small>' + Lang.get('messages.output') + '</small></label>' +
        '<select class="form-control" id="actions_output_id-' + counter + '" name="actions[' + counter + '][output_id]" required>' +
        '<option value=""></option>' +
        '</select>' +
        '</div>' +

        '<div class="col-lg-1">' +
        '<label><small>' + Lang.get('messages.outputValue') + '</small></label>' +
        '<select class="form-control" id="actions_output_value_onoff-' + counter + '" name="actions[' + counter + '][output_value]" required>' +
        '<option value="" ' + (vals.output_value == '' ? ' selected' : '') + '></option>' +
        '<option value="0"' + (vals.output_value == 0 ? ' selected' : '') + '>' + Lang.get('messages.toggleOff') + '</option>' +
        '<option value="1"' + (vals.output_value == 1 ? ' selected' : '') + '>' + Lang.get('messages.toggleOn') + '</option>' +
        '</select>' +
        '</div>' +

        '<div class="col-lg-1" style="padding-top: 24px;">' +
        '<button id="buttonActionRemove' + counter + '" class="btn btn-box btn-danger" type="button" onclick = "RemoveAction(this)">' +
        '<i class="fa fa-times"></i>' +
        '</button>' +

        '</div>' +

        '</div>' +
        '<hr>';

    div.innerHTML = html;

    document.getElementById("containerActions").appendChild(div);

    populateDropdown(vals.output_id, 'actions_output_id-' + counter, '/automation/outputsdropdown/' + $('[name=device_id]').val(), false, false);

    counter++;
}

function RemoveAction(div) {

    document.getElementById("containerActions").removeChild(div.parentNode.parentNode.parentNode);

}

function saveChanges() {

    if ($('#myForm').valid() == true) {

        var numOfConditions = $('[name^=conditions]').length;

        if (numOfConditions == 0) {
            alertGreska(Lang.get('messages.conditionsRequired'));
            return;
        }

        var numOfActions = $('[name^=actions]').length;

        if (numOfActions == 0) {
            alertGreska(Lang.get('messages.actionsRequired'));
            return;
        }

        $('#myModalIzmena').modal('show');
    }
}