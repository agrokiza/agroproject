var Broker_URL = 'wss://mqtt.stronganic.rs';

var userChange = true;

var options = {
    port: 443,
    //Read-only user
    username: 'reader@stronganic.rs',
    password: 'tWeqkop2Bn*',
    keepalive: 60
};

$(function () {

    var client = mqtt.connect(Broker_URL, options);

    client.on('connect', function () {
        client.subscribe(Topics);
        $.each(Devices, function (index, object) {
            publish(object, null, 'ping');
        });
    });

    client.on('message', function (topic, message) {

        var deviceUDI = topic.substr(0, topic.indexOf('/'));
        //console.log(deviceUDI);
        //Don't react on question message sent to device
        if (topic.indexOf('question') < 0) {
            $('#overlay' + deviceUDI).hide();
        }

        if (topic.indexOf('time') > 0) {
            alertInformacija(msgCurrenTime + message);
        }

        if (message.indexOf('inputevent') > 0) {
            return;
        }

        var identificator = topic.replace('/', '');

        //Process message and assign color according to min/max settings
        var currentContainer = $('#current' + identificator);
        currentContainer.text(message);
        if (parseFloat(message) > parseFloat(currentContainer.data('max'))) {
            currentContainer.css("color", "red");
        }
        else {
            if (parseFloat(message) < parseFloat(currentContainer.data('min'))) {
                currentContainer.css("color", "blue");
            }
            else {
                currentContainer.css("color", "black");
            }
        }

        var message_value = parseFloat(message);
        //Check for minimum and maximum

        if (message_value < parseFloat(Extremes['minimum' + identificator]) || Extremes['minimum' + identificator] === null) {
            Extremes['minimum' + identificator] = message_value;
            $('#minimum' + identificator).text(message);
        }

        if (message_value > parseFloat(Extremes['maximum' + identificator]) || Extremes['maximum' + identificator] === null) {
            Extremes['maximum' + identificator] = message_value;
            $('#maximum' + identificator).text(message);
        }

        //Set output control statuses
        var outControl = $('#output' + identificator);
        
        if(outControl.length>0){
            if (message == 'true') {
                userChange = false;
                outControl.bootstrapToggle('on');
                outControl.attr('checked', true);
            }
            else {
                if (message == 'false') {
                    userChange = false;
                    outControl.bootstrapToggle('off');
                    outControl.removeAttr('checked');
                }
            }
        }
        else{

            var digitalInputControl = $('#digitalInput' + identificator);
            if (message == 'true') {
                var indColor = digitalInputControl.attr('data-indcolor');

                if(indColor==''){
                    indColor = 'green';
                }
                digitalInputControl.css('background-color',indColor);
                digitalInputControl.addClass('circle-glow');
            }
            else {
                if (message == 'false') {
                    digitalInputControl.css('background-color','#d0d0d0');
                    digitalInputControl.removeClass('circle-glow');
                }
            }
        }




    });

    client.on('reconnect', function () {
        mqtt.connect(Broker_URL, options);
    });


    $('[id^=output]').bootstrapToggle({
        on: labelOn,
        off: labelOff,
        height: 35,
        onstyle: "success",
        offstyle: "danger"
    });

});

function outputPublish(object, device_id, output_id, active_rules) {

    var message = object.checked;

    active_rules=$('#widgetOutAuto'+output_id).is(':visible');

    //This prevent infinite loop when page ask device for status
    if (userChange == true) {
        if (active_rules == 0) {
            publish(device_id, output_id, message);
        }
        else {
            bootbox.confirm({
                message: "<div class='row center-row-my'>" +
                "<div class='col-sm-2 center-my'" +
                "<span style='font-size: 18px; font-weight: bold; color: black;'>" +
                "<i class='fa fa-question fa-3x' " +
                "aria-hidden='true' style='float: left;'></i></span></div> " +
                "<div class='col-sm-10 center-my'" +
                "<span style='font-size: 18px; font-weight: bold; color: black;'>"+ Lang.get('messages.outputManualChangeInAutoMode') + "</span></div> " +
                "</div>",
                size: 'normal',
                className: '',
                buttons: {
                    confirm: {
                        label: Lang.get('messages.yes'),
                        className: 'btn-danger'
                    },
                    cancel: {
                        label: Lang.get('messages.no'),
                        className: 'btn-success'
                    }
                },
                callback: function (result) {
                    if (result == true) {
                        //Disable all rules related to this output
                        $.get("../../../../automation/disableall/" + output_id, function (data) {
                            publish(device_id, output_id, message);

                            //Disable auto icon on all affected outputs
                            $.each(data, function (index, object) {
                                $('#widgetOutAuto' + object).fadeOut();
                            })

                        }).fail(function (data) {
                            alertGreska(data.responseText);
                        });
                    }
                    else {
                        userChange = false;
                        $(object).bootstrapToggle('toggle');
                    }
                }
            });
        }
    }
    userChange = true;
}

function publish(device_id, output_id, message) {
    $.get("../../../../mqtt/publish/" + device_id + "/" + output_id + "/" + message, function (data) {

    }).fail(function (data) {
        alertGreska(data.responseText);
    });
}

function refreshDevice(id, udi) {
    $('#overlay' + udi).show();
    publish(id, null, 'ping');
}

function deviceClock(id, udi) {
    publish(id, null, 'clock');
}

function openHistory(type, id) {

    //Remove tooltip before opening modal
    $('[data-toggle="tooltip"]').tooltip("hide");

    var container = $('#historyModalContainer');

    container.empty();

    if (type == "input") {
        $('#historyModalMain').attr("class", "modal-dialog modal-lg");
        var html = "<div class='chart'>" +
            "<div id='graph-container'>" +
            "<canvas id='historyChart' width='400' height='150'></canvas>" +
            "</div>" +
            "</div>";
        container.html(html);
        iscrtajGrafik(id);
    }
    else {
        $('#historyModalMain').attr("class", "modal-dialog modal-sm");
        var label = '';
        var url = '';
        if(type=="output"){
            label = labelOutputState;
            url="../../../history/output/" + id;
        }
        else{
            label = labelInputState;
            url="../../../history/digitalinput/" + id;
        }
        var html = '';
        html += "<table class='table table-responsive'><thead>" +
            "<tr>" +
            "<th>" +
            labelDateTime +
            "</th>" +
            "<th>" +
            label +
            "</th>" +
            "</tr>" +
            "</thead>" +
            "<tbody>";
        $.getJSON(url, function (result) {
            $.each(result['history'], function (i, field) {
                html += '<tr>' +
                    '<td>' +
                    field['datetime'] +
                    '</td>' +
                    '<td>' +
                    field['outputstate'] +
                    '</td>' +
                    '</tr>';
            });
            html += '</tbody></table>';
            container.html(html);
        }).fail(
            function () {
                html += '<tr>' +
                    '<td rowspan="2">Error</td>' +
                    '</tr>';
                html += '</tbody></table>';
                container.html(html);
            }
        );
    }


    $('#historyModal').modal('show');
}


function iscrtajGrafik(id) {

    var url = "../../../../history/input/" + id;
    $.getJSON(url, function (json) {

        var days = json['days'];
        var labels = [];
        var values = [];

        $.each(days, function (index, object) {
            labels[index] = object['date'];
            values[index] = object['avg_val'];
        });

        //Obrisi canvas kako bi izbega gomilanje grafika
        $('#historyChart').remove(); // this is my <canvas> element
        $('#graph-container').append('<canvas id="historyChart" width="400" height="150"></canvas>');

        var ctx = document.getElementById("historyChart").getContext("2d");

        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: labels,
                datasets: [{
                    label: json['label'] + " (" + labelDayAverage + ")",
                    backgroundColor: 'rgba(0, 128, 255, 1)',
                    borderColor: 'rgba(0, 128, 255, 1)',
                    data: values,
                    fill: false
                }]
            },
            options: {
                responsive: true,
                title: {
                    display: true,
                    text: labelChart
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: labelDate
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: labelValue
                        }
                    }]
                }
            }
        });

    }).fail(function () {
        console.log("Error");
    });


}


