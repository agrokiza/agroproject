
var table;
$(function () {
    table = $("#events_table").DataTable(
        {
            ajax: {
                url: "../../../events/get/1",
                dataSrc: 'events'
            },

            "processing": true,
            "order": [[ 7, "desc" ]],

            "columnDefs": [
                {
                    "targets": 0,
                    "data": 'devicedescription',
                    "defaultContent": ""
                },
                {
                    "targets": 1,
                    "data": 'devicelocation',
                    "defaultContent": ""
                },
                {
                    "targets": 2,
                    "data": 'inputdescription',
                    "defaultContent": ""
                },
                {
                    "targets": 3,
                    "data": 'type',
                    "defaultContent": ""
                },
                {
                    "targets": 4,
                    "data": 'set',
                    "defaultContent": ""
                },
                {
                    "targets": 5,
                    "data": 'read',
                    "defaultContent": ""
                },
                {
                    "targets": 6,
                    "data": 'created_at',
                    "defaultContent": ""
                },
                {
                    "targets": 7,
                    "data": 'updated_at',
                    "defaultContent": "",
                    "order":"desc"
                }
            ],

            //Cuvanje podesavanja tabele
            "stateSave": true,
            "stateSaveParams": function (settings, data) {
                data.search.search = "";
                //console.log(data);
            }

            ,
            //Iskljuci inicijalno sortiranje
            "language": {
                // TODO: Promeniti jezik!
                "url": jezik,
                "autoWidth": true
            }
        }
    )
    ;
});

function osveziTabelu(){
    var last = $('#last').val();
    table.ajax.url('../../../events/get/'+last).load();
}
