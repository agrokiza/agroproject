
var table;
$(function () {
    table = $("#devices_table").DataTable(
        {
            ajax: {
                url: "../../../automation/get",
                dataSrc: 'rules'
            },

            "processing": true,

            "columnDefs": [
                {
                    "targets": 0,
                    "data": 'description',
                    "defaultContent": ""
                },
                {
                    "targets": 1,
                    "data": 'status',
                    "defaultContent": ""
                },
                {
                    "targets": 2,
                    "data": null,
                    "defaultContent": "<a class='btn btn-link btn-link-MY' role='button' href='#' onclick='edit(this)'><i class='fa fa-pencil'></i></a>",
                    "orderable": false
                },
                {
                    "targets": 3,
                    "data": null,
                    "defaultContent": "<a class='btn btn-link btn-link-MY' role='button' data-toggle='modal' href='#myModal' onclick='deleteRecord(this)'><i class='fa fa-times'></i></a>",
                    "orderable": false
                }
            ],

            //Cuvanje podesavanja tabele
            "stateSave": true,
            "stateSaveParams": function (settings, data) {
                data.search.search = "";
                //console.log(data);
            }

            ,
            //Iskljuci inicijalno sortiranje
            "language": {
                "url": jezik,
                "autoWidth": true
            }
        }
    )
    ;
});

var idZaBrisanje = 0;
var row;

function deleteRecord(elem) {
    var data = table.row($(elem).parents('tr')).data();
    row = table.row($(elem).parents('tr'));
   
    idZaBrisanje = data['id'];
}

function edit(elem) {

    var data = table.row($(elem).parents('tr')).data();

    window.location = '../../../../automation/edit/' + data['id'];
}

function obrisi() {
    $.get("../../../automation/delete/" + idZaBrisanje, function (data) {
        row.remove().draw(false);
    }).fail(function () {
        alertGreska(deleteError)
    });

    $('#myModal').modal('hide');
}
