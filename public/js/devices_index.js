
var table;
$(function () {
    table = $("#devices_table").DataTable(
        {
            ajax: {
                url: "../../../devices/get",
                dataSrc: 'devices'
            },

            "processing": true,
            //Iskljuci sortiranje kolona
            "order": [2, 3],

            "columnDefs": [
                {
                    "targets": 0,
                    "data": 'userfullname',
                    "defaultContent": ""
                },
                {
                    "targets": 1,
                    "data": 'udi',
                    "defaultContent": ""
                },
                {
                    "targets": 2,
                    "data": 'description',
                    "defaultContent": ""
                },
                {
                    "targets": 3,
                    "data": 'last_activity',
                    "defaultContent": ""
                },
                {
                    "targets": 4,
                    "data": 'status',
                    "defaultContent": ""
                },
                {
                    "targets": 5,
                    "data": null,
                    "defaultContent": "<a class='btn btn-link btn-link-MY' role='button' href='#' onclick='edit(this)'><i class='fa fa-pencil'></i></a>",
                    "orderable": false
                },
                {
                    "targets": 6,
                    "data": null,
                    "defaultContent": "<a class='btn btn-link btn-link-MY' role='button' data-toggle='modal' href='#myModal' onclick='izmeniLink(this)'><i class='fa fa-times'></i></a>",
                    "orderable": false
                },
                {
                    "targets": 7,
                    "data": null,
                    "defaultContent": "<a class='btn btn-link btn-link-MY' role='button' href='#' onclick='setup(this)'><i class='fa fa-eye'></i></a>",
                    "orderable": false
                }
            ],

            //Cuvanje podesavanja tabele
            "stateSave": true,
            "stateSaveParams": function (settings, data) {
                data.search.search = "";
                //console.log(data);
            }

            ,
            //Iskljuci inicijalno sortiranje
            "language": {
                "url": jezik,
                "autoWidth": true
            }
        }
    )
    ;
});

var idZaBrisanje = 0;
var row;

function izmeniLink(elem) {
    var data = table.row($(elem).parents('tr')).data();
    row = table.row($(elem).parents('tr'));
   
    idZaBrisanje = data['id'];
}

function edit(elem) {

    var data = table.row($(elem).parents('tr')).data();

    window.location = '../../../../devices/edit/' + data['id'];
}

function setup(elem) {

    var data = table.row($(elem).parents('tr')).data();

    window.location = '../../../../devices/setup/' + data['id'];
}

function obrisi() {
    $.get("../../../devices/delete/" + idZaBrisanje, function (data) {
        row.remove().draw(false);
    }).fail(function () {
        alertGreska(deleteError)
    });

    $('#myModal').modal('hide');
}
