
function alertGreska(tekst){
    try {
        bootbox.hideAll();
        bootbox.alert({

            message: "<div class='row center-row-my'>" +
            "<div class='col-sm-2 center-my'" +
            "<span style='font-size: 18px; font-weight: bold; color: white;'>" +
            "<i class='fa fa-exclamation-triangle fa-3x' " +
            "aria-hidden='true' style='float: left;'></i></span></div> " +
            "<div class='col-sm-10 center-my'" +
            "<span style='font-size: 18px; font-weight: bold; color: white;'>"+tekst+ "</span></div> " +
            "</div>",
            size: 'normal',
            className: 'modal-danger',
            buttons: {
                ok: {
                    label: 'OK',
                    className: 'btn-success'
                }
            }
        });
    //    line-height: 1.5; display: inline-block; vertical-align: middle;
    }
    catch(err) {
        alert(tekst);
    }
}

function alertUspesno(tekst){
    try {
        bootbox.hideAll();
        bootbox.alert({
            message: "<div class='row center-row-my'>" +
            "<div class='col-sm-2 center-my'" +
            "<span style='font-size: 18px; font-weight: bold; color: green;'>" +
            "<i class='fa fa-check fa-3x' " +
            "aria-hidden='true' style='float: left;'></i></span></div> " +
            "<div class='col-sm-10 center-my'" +
            "<span style='font-size: 18px; font-weight: bold; color: green;'>"+tekst+ "</span></div> " +
            "</div>",
            size: 'normal',
            className: '',
            buttons: {
                ok: {
                    label: 'OK',
                    className: 'btn-success'
                }
            }
        });
    }
    catch(err) {
        alert(tekst);
    }
}

function alertInformacija(tekst){
    try {
        bootbox.hideAll();
        bootbox.alert({
            message:
            "<div class='row center-row-my'>" +
            "<div class='col-sm-2 center-my'" +
            "<span style='font-size: 18px; font-weight: bold; color: dimgrey;'>" +
            "<i class='fa fa-info fa-3x' " +
            "aria-hidden='true' style='float: left; padding-left: 20px;'></i></span></div> " +
            "<div class='col-sm-10 center-my'" +
            "<span style='font-size: 18px; font-weight: bold; color: dimgrey;'>"+tekst+ "</span></div> " +
            "</div>",
            size: 'normal',
            className: 'modal-default',
            buttons: {
                ok: {
                    label: 'OK',
                    className: 'btn-default'
                }
            }
        });
    }
    catch(err) {
        alert(tekst);
    }
}

function alertSure(tekst) {
    bootbox.hideAll();
    bootbox.confirm({
        message: "<div class='row center-row-my'>" +
        "<div class='col-sm-2 center-my'" +
        "<span style='font-size: 18px; font-weight: bold; color: black;'>" +
        "<i class='fa fa-question fa-3x' " +
        "aria-hidden='true' style='float: left;'></i></span></div> " +
        "<div class='col-sm-10 center-my'" +
        "<span style='font-size: 18px; font-weight: bold; color: black;'>"+tekst+ "</span></div> " +
        "</div>",
        size: 'normal',
        className: '',
        buttons: {
            confirm: {
                label: Lang.get('messages.yes'),
                className: 'btn-success'
            },
            cancel: {
                label: Lang.get('messages.no'),
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if(result==true){
               
            }
        }
    });

}