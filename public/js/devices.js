var counter = 1;

function AddOutput(parameters) {

    var params = $.extend({
        id: '',
        description: '',
        type_id: ''
    }, parameters);

    var div = document.createElement('DIV');
    var imediva = "OutputDiv" + counter
    div.id = imediva;
    //div.className = "row";
    var html = '' +
        '<div class="row">' +
        '<div class="col-lg-3">' +
        otCombo.replace('type_id', 'outputs[' + counter + '][type_id]').replace('idtypeid', 'type_id' + counter) +
        '</div> ' +

        '<div class="col-lg-3">' +
        '<input class="form-control" type="text" name="outputs[' + counter + '][description]" placeholder="' + labelDescription + '" value="' + params.description + '">' +
        '</div>' +


        '<div class="col-lg-2">' +
        '<button id="buttonDetailRemove' + counter + '" class="btn btn-box btn-danger" type="button" onclick = "RemoveOutput(this)">' +
        '<i class="fa fa-times"></i>' +
        '   </button>' +

        '</div>' +

        '</div>' +
        '<hr>';

    if (params.id != '') {
        html = '<input type="hidden" value="' + params.id + '" name="outputs[' + counter + '][id]">' + html;
    }

    div.innerHTML = html;

    document.getElementById("containerOutputs").appendChild(div);

    $('#type_id' + counter + '>option:eq(' + params.type_id + ')').attr('selected', true);

    counter++;
}

function RemoveOutput(div) {

    document.getElementById("containerOutputs").removeChild(div.parentNode.parentNode.parentNode);

}

function AddInput(parameters) {

    var params = $.extend({
        id: '',
        description: '',
        unit_id: '',
        min: '',
        max: ''
    }, parameters);

    var div = document.createElement('DIV');
    var imediva = "InputDiv" + counter;
    div.id = imediva;
    //div.className = "row";
    var html = '' +
        '<div class="row">' +
        '<div class="col-lg-3">' +
        iuCombo.replace('unit_id', 'inputs[' + counter + '][unit_id]').replace('idunitid', 'unit_id' + counter) +
        '</div> ' +

        '<div class="col-lg-3">' +
        '<input class="form-control" type="text" name="inputs[' + counter + '][description]" placeholder="' + labelDescription + '" value="' + params.description + '">' +
        '</div>' +

        '<div class="col-lg-2">' +
        '<input class="form-control" id="inputsMin' + counter + '" type="number" step="0.01" name="inputs[' + counter + '][min]" placeholder="' + labelMin + '" value="' + params.min + '">' +
        '</div>' +

        '<div class="col-lg-2">' +
        '<input class="form-control" id="inputsMax' + counter + '" type="number" step="0.01" name="inputs[' + counter + '][max]" placeholder="' + labelMax + '" value="' + params.max + '">' +
        '</div>' +

        '<div class="col-lg-2">' +
        '<button id="buttonDetailRemove' + counter + '" class="btn btn-box btn-danger" type="button" onclick = "RemoveInput(this)">' +
        '<i class="fa fa-times"></i>' +
        '   </button>' +

        '</div>' +

        '</div>' +
        '<hr>';

    if (params.id != '') {
        html = '<input type="hidden" value="' + params.id + '" name="inputs[' + counter + '][id]">' + html;
    }

    div.innerHTML = html;
    document.getElementById("containerInputs").appendChild(div);

    var unIDcombo = $('#unit_id' + counter);
    unIDcombo.on('change', function () {
        var unID = parseInt(this.value);
        var inID = this.id;
        inID = inID.replace('unit_id','');

        if (unID == 6) {
            $('#inputsMin' + inID).hide();
            $('#inputsMax' + inID).hide();
        }
        else {
            $('#inputsMin' + inID).show();
            $('#inputsMax' + inID).show();
        }
    });

    $('#unit_id' + counter + ' >option:eq(' + params.unit_id + ')').attr('selected', true);

    unIDcombo.trigger('change');

    counter++;
}

function RemoveInput(div) {

    document.getElementById("containerInputs").removeChild(div.parentNode.parentNode.parentNode);

}