<?php
use Illuminate\Support\Facades\Cookie;

//!!!!!!!!!!!!!!!!!! PRIORITET !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//TODO: Event trigger za obican input (mogucnost definisanja da li se dize event na true ili na false) (admin + korisnik)
//TODO: Hendlovanje obicnih inputa u automatizacionim pravilima
//TODO: Ne osvezava settings.json????????????????
//TODO: EVENT za reset uredjaja !!!!!!!!!!!!!!!!!!!!
//TODO: Ne izracunava dobro threshold
//TODO: Monitoring temperature procesora
//*******************************************************************************************************

// TODO: Obratiti pažnju da kada se pojave vezani podaci za korisnika, treba umesto brisanja korisnika bez ikakve provere, samo da se korisnik deaktivira!!!
// TODO: Neophodno je UVEK umesto btn-primary koristiti btn-primary btn-primary-my koji je definisan u public/AdminLTE/build/less/buttons.less!!!

//TODO: Format topica: udi_uredjaja/uui_velicine/vrednost

//TODO: Kako bezbedno generisati i cuvati sifru korisnika za MQTT?

//Kako izvesti samopodesavanje uredjaja?
//1. Prilikom startovanja uredjaja/skripte, salje se poruka getsettings
//2. Node skripta prepoznaje tu poruku i poziva rutu na serveru koja emituje poedsavanja uredjaja u JSON formatu (ova metoda treba da bude zasticena tako da samo sa jedne IP adrese moze biti pozvana)
//3. Uredjaj prihvata emitovane poruke i smesta ih u settings file
//Ista procedura se primenjuje kada korisnik promeni min ili max


//TODO: Mehanizam provere koje su vrednosti prihvacene od strane uredjaja

//Notifikacije i eventi!!!!!!!!!!!!!!!!!!!
//Event se kreira iz putem odredjene rute
//Event ima vek trajanja 1 sat i ne mogu biti kreirana 2 eventa istog tipa u tom razdoblju
//Svi eventi koji nisu updejtovani vise od 2 sata (120 minuta) vremena obelezavaju se kao neaktivni
//Vreme trajanja eventa je definisano u config/inputevents

//TODO: Kako biti siguran da je uredjaj primio poruku da nesto uradi? Kako obavestiti korisnika da uredjaj nije primio poruku? Da li vrsiti proveru dostupnosti uredjaja pre samog slanja?

//TODO: Rasporedjivanje elemenata po zelji korisnika

//TODO: Dodati push button (podesavanje vremena trajanja impulsa)



//TODO: Detaljne statistike

//TODO: IF this than that logika (razmisli kako resiti problem vremenskih rasporeda, npr. ukljuci samo posle 18h)
//Uvesti dve vrste: 1. regular, 2. roaming
//1. Regular: omogucava programiranje ulaza i izlaza samo u okviru jednog uredjaja
//2. Roaming: omogucava interakciju ulaza i izlaza u okviru svih uredjaja datog korisnika
//Roaming se obavlja posredstvom servera uz proveru da li uredjaji pripadaju istom korisniku
//Roaming je dostupan samo odredjenim korisnickim paketima
//Za pocetak implemetirati najednostavniju interakciju izlaza i ulaza + vremensko programiranje
//Kombinovanje vise uslova pomocu operatora (AND, OR, XOR)
//Mogucnost dodavanja vise izlaznih vrednosti za jedno IFTTT pravilo
//Mogucnost sagledavanja prosecnih vrednosti vise senzora
//Dodati mogucnost odabira odstupanja vrednosti za svaki uslov/pravilo (0-5%)


//Obavestavanje korisnika (sms, e-mail, push-notifikacije)

//TODO: Mobilna aplikacija

//HARDVERSKI DEO******************************************

//TODO: Prekidac za rucni mod upravljanja koji ce omoguciti upravljanje cak iako softver pukne

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/phpinfo',function(){
    phpinfo();
});*/

Route::get('/','WelcomeController@index')->name('home');

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

Route::get('setlocale/{locale}', function ($locale) {

    $cookie=Cookie::forever('locale', $locale);

    return redirect('/')->withCookie($cookie);
});
//***************************USERS********************************************************************************************
Route::get('/users', 'KorisniciController@index')->name('users.all');
Route::get('/users/delete/{id}', 'KorisniciController@delete')->name("users.delete");
Route::get('/users/edit/{id}', 'KorisniciController@edit')->name("users.edit");
Route::patch('/users/update/{id}', 'KorisniciController@update')->name("users.update");
Route::get('/users/activatedeactivate/{id}', 'KorisniciController@activatedeactivate')->name("users.activatedeactivate");

Route::get('/users/credentials/changepassword','KorisniciController@credentials')->name('users.credentials');
Route::post('/users/credentials/changepassword','KorisniciController@postCredentials')->name('users.credentials.changepassword');

//***************************END USERS********************************************************************************************


//***************************MQTT LOGS********************************************************************************************

Route::get('/mqttlogs', 'MqttLogsController@index')->name('mqttlogs.all');
Route::get('/mqttlogs/get/{last?}', 'MqttLogsController@get')->name('mqttlogs.get');

//***************************END MQTT LOGS********************************************************************************************


//***************************DEVICES********************************************************************************************

Route::get('/devices', 'DevicesController@index')->name('devices.all');
Route::get('/devices/get/{last?}', 'DevicesController@get')->name('devices.get');

Route::get('/devices/add', 'DevicesController@add')->name('devices.add');
Route::post('/devices/add', 'DevicesController@create')->name('devices.create');

Route::get('/devices/delete/{id}', 'DevicesController@delete')->name('devices.delete');

Route::get('/devices/edit/{id}', 'DevicesController@edit')->name("devices.edit");
Route::patch('/devices/update/{id}', 'DevicesController@update')->name("devices.update");

Route::get('/devices/setup/{id}', 'DevicesController@setup')->name('devices.setup');

//This is used to allow users to change device name, output name and background color
Route::get('/devices/customize/{id}', 'DevicesCustomizeController@customize')->name("devices.customize");
Route::patch('/devices/customize/{id}', 'DevicesCustomizeController@customizePost')->name("devices.customizePost");

Route::get('/devices/customize/showhide/{type}/{id}/{state}','DevicesCustomizeController@showhide');
//***************************END DEVICES********************************************************************************************


//***************************MQTT PUBLISH********************************************************************************************

Route::get('/mqtt/publish/{device_id}/{output_id?}/{message}', 'MqttPublisherController@publish')->name('mqttlogs.publish');

Route::get('/mqtt/getsettings/{device_id}', 'MqttPublishSettingsController@publishSettings');

Route::post('/mqtt/inputevent/{uui}', 'MqttPublishSettingsController@createEvent');

//***************************END MQTT PUBLISH********************************************************************************************


//***************************A U T O M A T I O N********************************************************************************************

Route::get('/automation', 'AutomationController@index')->name('automation.all');

Route::get('/automation/get/{option?}', 'AutomationController@get');

Route::get('/automation/logicaloperatorsdropdown','AutomationController@logicaloperatorsdropdown');

Route::get('/automation/conditiontypesdropdown','AutomationController@conditiontypesdropdown');

Route::get('/automation/inputsdropdown/{device_id}','AutomationController@inputsdropdown');

Route::get('/automation/outputsdropdown/{device_id}','AutomationController@outputsdropdown');

Route::get('/automation/comparisonoperatorsdropdown','AutomationController@comparisonoperatorsdropdown');

Route::get('/automation/add', 'AutomationController@add')->name('automation.add');
Route::post('/automation/add', 'AutomationController@create')->name('automation.create');

Route::get('/automation/edit/{id}', 'AutomationController@edit')->name('automation.edit');
Route::patch('/automation/edit/{id}', 'AutomationController@update')->name('automation.update');

Route::get('/automation/delete/{id}', 'AutomationController@delete');

Route::get('/automation/disableall/{output_id}', 'AutomationController@disableall');
//***************************E N D    A U T O M A T I O N********************************************************************************************


//***************************E V E N T S********************************************************************************************

Route::get('/events', 'EventController@index')->name('events.all');

Route::get('/events/get/{option}', 'EventController@get');

Route::get('/events/inputevent/{device_id}/{uui}/{message}', 'CreateEventController@createEvent');

//***************************E N D    E V E N T S********************************************************************************************


//***************************HISTORY********************************************************************************************

Route::get('/history/output/{id}', 'HistoryController@output')->name('history.output');

Route::get('/history/input/{id}', 'HistoryController@input')->name('history.input');

Route::get('/history/digitalinput/{id}', 'HistoryController@digitalinput')->name('history.digitalinput');

//***************************END HISTORY********************************************************************************************


//Route::get('/test/{user_id}','MqttPublisherController@publishRules');