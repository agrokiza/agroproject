<!-- History Modal -->
<div id="historyModal" class="modal" role="dialog">
    <div id="historyModalMain" class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">{{trans('messages.history')}}</h4>
            </div>
            <div id="historyModalContainer" class="modal-body" style="overflow-y: scroll; max-height: 600px;">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('messages.close')}}</button>
            </div>
        </div>

    </div>
</div>

<div class='col-md-12'>

    <?php
    $devices = \App\Device::where('user_id', Auth::id())->where('is_active', 1)->get();
    $extremes = [];
    $alldevices = [];
    $topics = [];
    ?>
    @foreach ($devices as $device)
        <div id="deviceBox{{$device->id}}" class="box"
             style="background-color: #e7d4c4; display: {{$device->ui_show==1? 'block':'none'}};">

            <div class="box-header with-border">

                <div class="row">
                    <div class="col-lg-9 col-md-9 col-sm-8">
                        <h3 class="box-title" style="font-size: 24px;"><b>{{$device->description}}</b></h3>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4">
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-toggle="tooltip"
                                    title="{{trans('messages.refreshDevice')}}"
                                    onclick="refreshDevice('{{$device->id}}','{{$device->udi}}')"><i
                                        style="color: #444444;"
                                        class="fa fa-refresh fa-2x"></i>
                            </button>
                            &nbsp;&nbsp;|&nbsp;&nbsp;
                            <button type="button" class="btn btn-box-tool" data-toggle="tooltip"
                                    title="{{trans('messages.deviceClock')}}"
                                    onclick="deviceClock('{{$device->id}}','{{$device->udi}}')"><i
                                        style="color: #444444;"
                                        class="fa fa-clock-o fa-2x"></i>
                            </button>
                            &nbsp;&nbsp;|&nbsp;&nbsp;
                            <a type="button" class="btn btn-box-tool" data-toggle="tooltip"
                               title="{{trans('messages.customizeDevice')}}"
                               href="{{route('devices.customize',$device->id)}}"><i style="color: #444444;"
                                                                                    class="fa fa-cog fa-2x"></i>
                            </a>
                            &nbsp;&nbsp;|&nbsp;&nbsp;
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                                    title="{{trans('messages.minimize')}}"><i style="color: #444444;"
                                                                              class="fa fa-minus fa-2x"></i>
                            </button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                </div>
            </div>

            <div class="box-body">

                <?php
                array_push($topics, $device->udi . '/#');
                array_push($alldevices, $device->id);
                ?>

                <div id="a{{$device->udi}}" class="row">
                    <?php
                    $inputs = $device->input;
                    ?>
                    @foreach($inputs as $input)
                        @if($input->unit_id!=6)
                            <?php
                            $min = \App\MqttMessage::where('device_id', '=', $device->id)->where('type', 'i')->where('unit_id', $input->id)->whereDate('created_at', Carbon\Carbon::today())->min(DB::raw('CAST(message AS DOUBLE)'));
                            $max = \App\MqttMessage::where('device_id', '=', $device->id)->where('type', 'i')->where('unit_id', $input->id)->whereDate('created_at', Carbon\Carbon::today())->max(DB::raw('CAST(message AS DOUBLE)'));

                            $extremes['minimum' . $device->udi . $input->uui] = $min;
                            $extremes['maximum' . $device->udi . $input->uui] = $max;
                            ?>
                            <div id="widgetIn{{$input->id}}" class='col-lg-4 col-md-6 col-sm-6 col-xs-12'
                                 style="display: {{$input->ui_show==1? 'block':'none'}};">
                                <!-- Widget: user widget style 1 -->
                                <div class="box box-widget widget-user">
                                    <!-- Add the bg color to the header using any of the bg-* classes -->
                                    <div class="widget-user-header {{$input->inputdata['class']}}"
                                         style="{{$input->ui_bg_color!=null? 'background-color: '.$input->ui_bg_color.' !important; color: black !important;':''}}">
                                        <h3 class="widget-user-username">{{$input->inputdata['name']}}
                                            <a class="pull-right" style="color: darkslategrey;" href="#"
                                               onclick="openHistory('input','{{$input->id}}')" data-toggle="tooltip"
                                               title="{{trans('messages.history')}}"><i
                                                        class="fa fa-line-chart"></i></a>
                                        </h3>
                                        <h5 class="widget-user-desc"><b>{{$input->description}}</b></h5>
                                    </div>

                                    <div class="box-footer">
                                        <div class="row">
                                            <div class="col-sm-4 border-right">
                                                <div class="description-block">
                                                    <h5 id="minimum{{$device->udi}}{{$input->uui}}"
                                                        class="description-header">{{$min}}</h5>
                                                    <span class="description-text">{{trans('messages.dayMinimum')}}</span>
                                                </div>
                                                <!-- /.description-block -->
                                            </div>
                                            <!-- /.col -->
                                            <div class="col-sm-4 border-right">
                                                <div class="description-block">
                                                    <h1 id="current{{$device->udi}}{{$input->uui}}"
                                                        data-min="{{$input->min}}"
                                                        data-max="{{$input->max}}">{{trans('messages.errorNotAvailable')}}</h1>
                                                    <span class="description-text">{{trans('messages.currentValue')}}</span>
                                                </div>
                                                <!-- /.description-block -->
                                            </div>
                                            <!-- /.col -->
                                            <div class="col-sm-4">
                                                <div class="description-block">
                                                    <h5 id="maximum{{$device->udi}}{{$input->uui}}"
                                                        class="description-header">{{$max}}</h5>
                                                    <span class="description-text">{{trans('messages.dayMaximum')}}</span>
                                                </div>
                                                <!-- /.description-block -->
                                            </div>
                                            <!-- /.col -->
                                        </div>
                                        <!-- /.row -->
                                    </div>

                                </div>
                                <!-- /.widget-user -->
                            </div><!-- /.col -->

                        @else
                            <div id="widgetDigitalIn{{$input->id}}" class="col-lg-4 col-md-6 col-sm-6 col-xs-12"
                                 style="display: {{$input->ui_show==1? 'block':'none'}};">

                                <div class="info-box">
                                    <div class="info-box-icon {{$input->inputdata['class']}}"
                                         style="line-height: 80px; {{$input->ui_bg_color!=null? 'background-color: '.$input->ui_bg_color.' !important;':''}}">

                                        <div id="digitalInput{{$device->udi.$input->uui}}" data-id="{{$input->id}}" data-indcolor="{{$input->ui_ind_color}}" class="circle">

                                        </div>

                                    </div>

                                    <div class="info-box-content">
                                        <a class="pull-right" style="color: darkslategrey; font-size: 24px;" href="#"
                                           onclick="openHistory('digitalinput','{{$input->id}}')" data-toggle="tooltip"
                                           title="{{trans('messages.history')}}"><i class="fa fa-history"></i></a>

                            <span class="info-box-text">
                                {{$input->inputdata['name']}}
                            </span>
                                    <span class="info-box-number">
                                        {{$input->description}}
                                    </span>

                                    </div>

                                </div>
                                <!-- /.info-box -->


                            </div>

                        @endif

                    @endforeach
                </div>

                <div id="b{{$device->udi}}" class="row">
                    <?php
                    $outputs = $device->output;
                    ?>
                    @foreach($outputs as $output)
                        <div id="widgetOut{{$output->id}}" class="col-lg-4 col-md-6 col-sm-6 col-xs-12"
                             style="display: {{$output->ui_show==1? 'block':'none'}};">

                            <div class="info-box">
                                <div class="info-box-icon {{$output->outputdata['class']}}"
                                     style="line-height: 80px; {{$output->ui_bg_color!=null? 'background-color: '.$output->ui_bg_color.' !important;':''}}">
                                    {!! Form::checkbox('output'.$output->uui,1,$output->status,array('id'=>'output'.$device->udi.$output->uui,'data-id'=>$output->id,'data-activerules'=>$output->activerules,'onchange'=>'outputPublish(this,'.$device->id.','.$output->id.','.$output->activerules.')')) !!}
                                </div>

                                <div class="info-box-content">
                                    <a class="pull-right" style="color: darkslategrey; font-size: 24px;" href="#"
                                       onclick="openHistory('output','{{$output->id}}')" data-toggle="tooltip"
                                       title="{{trans('messages.history')}}"><i class="fa fa-history"></i></a>

                            <span class="info-box-text">
                                {{$output->outputdata['name']}}
                            </span>
                                    <span class="info-box-number">
                                        {{$output->description}}
                                        @if($output->activerules==1)
                                            <br><i id="widgetOutAuto{{$output->id}}"
                                                   class="fa fa-magic fa-lg pull-right"
                                                   style="color: #8CB336;" data-toggle="tooltip"
                                                   title="{{trans('messages.outputInAutoMode')}}"></i>
                                        @endif
                                    </span>

                                </div>

                            </div>
                            <!-- /.info-box -->


                        </div>

                    @endforeach
                </div>

            </div><!--Device box-body end-->
            <div id="overlay{{$device->udi}}" class="overlay" style="display: block;">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div><!--Device box end-->
    @endforeach

</div><!-- /.col -->

@section('header')
    <link href="{{asset('bootstrap-toggle/css/bootstrap-toggle.min.css')}}" rel="stylesheet">
    <style>
        .circle{
            border-radius: 50%;
            width: 40px;
            height: 40px;
            background-color: #d0d0d0;
            margin: 24px;
            border: solid 1px darkslategrey;
        }
        .circle-glow{
            box-shadow:
                    0 0 20px 10px #fff  /* inner white */
        }
    </style>
@endsection

@section('scripts')
    <script src="{{asset('mqtt/dist/mqtt.min.js')}}"></script>
    <script src="{{asset('bootstrap-toggle/js/bootstrap-toggle.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.bundle.min.js"></script>
    <script>
        var Topics = {!!json_encode($topics)!!};
        var Extremes = {!!json_encode($extremes)!!};
        var Devices = {!!json_encode($alldevices)!!};

        var labelOn = '{{trans('messages.toggleOn')}}';
        var labelOff = '{{trans('messages.toggleOff')}}';

        var labelOutputState = '{{trans('messages.outputState')}}';
        var labelInputState = '{{trans('messages.inputState')}}';
        var labelDateTime = '{{trans('messages.dateTime')}}';
        var labelDate = '{{trans('messages.date')}}';
        var labelValue = '{{trans('messages.value')}}';
        var labelChart = '{{trans('messages.monthlyChart')}}';
        var labelDayAverage = '{{trans('messages.dayAverage')}}';
        var msgCurrenTime = '{{trans('messages.currentDeviceTime')}}';

    </script>
    <script src="{{asset('js/user_control_panel.js')}}"></script>
@endsection