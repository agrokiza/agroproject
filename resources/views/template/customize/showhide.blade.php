<link rel="stylesheet" href="{{ asset("vakata-jstree/dist/themes/proton/style.min.css") }}">

<h3 class="control-sidebar-heading">{{trans('messages.showHideElements')}}</h3>
<?php
$devices = \App\Device::where('user_id', Auth::id())->get();
?>
<div id="showHideTree">
    <ul>
        @foreach($devices as $device)
            <?php
            $inputs = $device->input;
            $outputs = $device->output;
            ?>
            <?php
            if ($device->ui_show == 1) {
                $selected1 = 'true';
            } else {
                $selected1 = 'false';
            }
            echo $selected1;
            ?>

            <li data-jstree='{"opened":true,"selected":{{$selected1}}, "icon" : "glyphicon glyphicon-hdd"}' data-litype="device"
                data-liid="{{$device->id}}">{{$device->description}}
                <ul>
                    @if(count($inputs)>0)
                        <li data-jstree='{"opened":true, "checkbox_disabled":true, "icon" : "glyphicon glyphicon-list-alt"}'>{{trans('messages.inputs')}}
                            <ul>
                                @foreach($inputs as $input)
                                    <?php
                                    if ($input->ui_show == 1) {
                                        $selected2 = 'true';
                                    } else {
                                        $selected2 = 'false';
                                    }
                                    ?>
                                    <li data-jstree='{"opened":true,"selected":{{$selected2}}, "icon" : "glyphicon glyphicon-save"}' data-litype="input"
                                        data-liid="{{$input->id}}">{{$input->description}}</li>
                                @endforeach
                            </ul>
                        </li>
                    @endif

                    @if(count($outputs)>0)
                        <li data-jstree='{"opened":true, "checkbox_disabled":true, "icon" : "glyphicon glyphicon-list-alt"}'>{{trans('messages.outputs')}}
                            <ul>
                                @foreach($outputs as $output)
                                    <?php
                                    if ($output->ui_show == 1) {
                                        $selected3 = 'true';
                                    } else {
                                        $selected3 = 'false';
                                    }
                                    ?>
                                    <li data-jstree='{"opened":true,"selected":{{$selected3}}, "icon" : "glyphicon glyphicon-open"}' data-litype="output"
                                        data-liid="{{$output->id}}">{{$output->description}}</li>
                                @endforeach
                            </ul>
                        </li>
                    @endif
                </ul>
            </li>
        @endforeach
    </ul>
</div>
<!-- /.control-sidebar-menu -->

<script src="{{asset('vakata-jstree/dist/jstree.min.js')}}"></script>
<script>
    $(function () {

        $('#showHideTree').on('select_node.jstree deselect_node.jstree', function (evt, data) {
            var nod = $(data.node)[0];
            var type = nod.data.litype;
            var id = nod.data.liid;
            var state = nod.state.selected;

            var selObj;

            if (type == "device") {
                selObj = $('#deviceBox' + id);
            }
            else {
                if (type == "input") {
                    selObj = $('#widgetIn' + id);
                }
                else {
                    if (type == "output") {
                        selObj = $('#widgetOut' + id);
                    }
                }
            }

            var link = '../../devices/customize/showhide/'+type+'/'+id+'/'+state;
            $.get(link,function(){
                if(state==true){
                    selObj.show();
                }
                else{
                    selObj.hide();
                }

            }).fail(function (data) {
                alertGreska(data.responseText);
            });



        }).jstree({
            'core': {
                'themes': {
                    'name': 'proton',
                    'responsive': true,
                    'expand_selected_onload': true
                }
            },
            "checkbox": {
                "keep_selected_style": false,
                "cascade": "",
                "three_state": false
            },
            "plugins": ["wholerow", "checkbox"]
        });

        $('.jstree-checkbox-disabled').hide();
    });
</script>
