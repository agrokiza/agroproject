<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <br>
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">{{ trans('messages.menuTitle') }}</li>

            <!-- Optionally, you can add icons to the links -->
            @if(Auth::user()->role == 'admin')
                <li class="{{ Request::is('users*') ? 'active' : '' }}"><a href="{{route('users.all')}}"><i
                                class="fa fa-users"></i> <span>{{ trans('messages.users') }}</span></a></li>

                <li class="{{ Request::is('devices*') ? 'active' : '' }}"><a href="{{route('devices.all')}}"><i
                                class="fa fa-laptop"></i> <span>{{ trans('messages.menuDevices') }}</span></a></li>

                <li class="{{ Request::is('mqttlogs*') ? 'active' : '' }}"><a href="{{route('mqttlogs.all')}}"><i
                                class="fa fa-list"></i> <span>{{ trans('messages.menuMQTTLogs') }}</span></a></li>
            @endif

            @if(Auth::user()->role == 'user')
                <li class="{{ Request::is('automation*') ? 'active' : '' }}"><a href="{{route('automation.all')}}"><i
                                class="fa fa-magic"></i> <span>{{ trans('messages.automation') }}</span></a>
                </li>

                <?php
                $dids = \App\Device::where('user_id', Auth::id())->get(['id']);
                $di = [];
                foreach ($dids as $d) {
                    array_push($di, $d['id']);
                }
                $inputs = \App\Input::whereIn('device_id', $di)->count();
                ?>
                @if($inputs>0)
                    <li class="{{ Request::is('events*') ? 'active' : '' }}"><a href="{{route('events.all')}}"><i
                                    class="fa fa-exclamation-triangle"></i> <span>{{ trans('messages.events') }}</span></a>
                    </li>
                @endif
            @endif

            {{--<li><a href="#"><i class="fa fa-link"></i> <span>Another Link</span></a></li>
            <li class="treeview">
                <a href="#"><i class="fa fa-link"></i> <span>Multilevel</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="#">Link in level 2</a></li>
                    <li><a href="#">Link in level 2</a></li>
                </ul>
            </li>--}}
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>