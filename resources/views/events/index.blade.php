@extends('template.template')

@section('headercss')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('header')
    <link href="{{ asset("/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}" rel="stylesheet">
@stop


@section('scripts')
    <script src="{{ asset("/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}"></script>
    <script src="{{ asset("/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")}}"></script>

    <!-- FastClick -->
    <script src="{{ asset("/AdminLTE/plugins/fastclick/fastclick.js")}}"></script>

    <script>
        var jezik = "{{ asset(trans('messages.prevodDataTables'))}}";
    </script>

    <!-- page script -->
    <script src="{{asset("/js/events.js")}}"></script>

@stop

@section('content')

    <div class="box">

        <div class="box-body">

            <div class="row">

                <div class="col-sm-12">
                    <div class="form-group">
                        <label>{{trans('messages.eventStatus')}}</label>
                        <select id="last" onchange="osveziTabelu()" class="form-control">
                            <option value="1" selected>{{trans('messages.activeEvents')}}</option>
                            <option value="0">{{trans('messages.inactiveEvents')}}</option>
                            <option value="2">{{trans('messages.allEvents')}}</option>
                        </select>
                    </div>
                    <div class="table-responsive" style="min-height: 300px">
                        <table id="events_table" class="table table-bordered table-striped dataTable" width="100%"
                               role="grid" aria-describedby="example1_info">
                            <thead>
                            <tr role="row">

                                <th aria-label="{{trans('messages.clickToSort')}}">{{trans('messages.device')}}
                                </th>

                                <th aria-label="{{trans('messages.clickToSort')}}">{{trans('messages.location')}}
                                </th>

                                <th aria-label="{{trans('messages.clickToSort')}}">{{trans('messages.description')}}
                                </th>

                                <th aria-label="{{trans('messages.clickToSort')}}">{{trans('messages.type')}}
                                </th>

                                <th aria-label="{{trans('messages.clickToSort')}}">{{trans('messages.set')}}
                                </th>

                                <th aria-label="{{trans('messages.clickToSort')}}">{{trans('messages.read')}}
                                </th>

                                <th aria-label="{{trans('messages.clickToSort')}}">{{trans('messages.createdAt')}}
                                </th>

                                <th aria-label="{{trans('messages.clickToSort')}}">{{trans('messages.updatedAt')}}
                                </th>

                            </tr>
                            </thead>

                            <tbody>


                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection