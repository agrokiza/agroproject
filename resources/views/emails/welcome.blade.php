<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

    {{--<link href="{{ asset('css/emailsstyle.css') }}" rel="stylesheet" type="text/css" >--}}

    <style type="text/css" rel="stylesheet" media="all">
        /* Media Queries */
        @media only screen and (max-width: 500px) {
            .button {
                width: 100% !important;
            }
        }
    </style>

    <style type="text/css">
        /* Layout ------------------------------ */
        body {
            margin: 0px;
            padding: 0px;
            width: 100%;
            background-color: #F2F4F6;
        }

        .email-wrapper {
            width: 100%;
            margin: 0;
            padding: 0;
            background-color: #8cb336;
        }

        /* Masthead ----------------------- */

        .email-masthead {
            padding: 25px 0;
            text-align: center;
        }

        .email-masthead_name {
            font-size: 16px;
            font-weight: bold;
            color: #FFFFFF;
            text-decoration: none;
        }

        .email-body {
            width: 100%;
            margin: 0;
            padding: 0;
            border-top: 1px solid #EDEFF2;
            border-bottom: 1px solid #EDEFF2;
            background-color: #FFF;
        }

        .email-body_inner {
            width: auto;
            max-width: 570px;
            margin: 0 auto;
            padding: 0;
        }

        .email-body_cell {
            padding: 35px;
        }

        .email-footer {
            width: auto;
            max-width: 570px;
            margin: 0 auto;
            padding: 0;
            text-align: center;
            color: #FFFFFF;
        }

        .email-footer_cell {
            color: #FFFFFF;
            padding: 35px;
            text-align: center;
        }

        /* Body ------------------------------ */

        .body_action {
            width: 100%;
            margin: 30px auto;
            padding: 0;
            text-align: center;
        }

        .body_sub {
            margin-top: 25px;
            padding-top: 25px;
            border-top: 1px solid #EDEFF2;
        }

        /* Type ------------------------------ */

        .anchor {
            color: #3869D4;
        }

        .header-1 {
            margin-top: 0;
            color: #2F3133;
            font-size: 19px;
            font-weight: bold;
            text-align: left;
        }

        .paragraph {
            margin-top: 0;
            color: #74787E;
            font-size: 16px;
            line-height: 1.5em;
        }

        .paragraph-important {
            color: red;
            padding-left: 5px;
        }

        .paragraph-sub {
            margin-top: 0;
            color: #74787E;
            font-size: 12px;
            line-height: 1.5em;
        }

        .paragraph-sub-footer {
            margin-top: 0;
            color: #FFFFFF;
            font-size: 12px;
            line-height: 1.5em;
        }

        .paragraph-center {
            text-align: center;
        }

        /* Buttons ------------------------------ */
        .button {
            display: block;
            display: inline-block;
            width: 200px;
            min-height: 20px;
            padding: 10px;
            background-color: #3869D4;
            border-radius: 3px;
            color: #ffffff;
            font-size: 15px;
            line-height: 25px;
            text-align: center;
            text-decoration: none;
            -webkit-text-size-adjust: none;
        }

        .button--green {
            background-color: #9bc53d;
        }

        .button--red {
            background-color: #dc4d2f;
        }

        .button--blue {
            background-color: #3869D4;
        }

    </style>

</head>

<?php $fontFamily = 'font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;'; ?>

<body>

<table width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td class="email-wrapper" align="center">
            <table width="100%" cellpadding="0" cellspacing="0">
                <!-- Logo -->
                <tr>
                    <td class="email-masthead">
                        <!-- The logo image in the navbar-->
                        <a style="{{ $fontFamily }}" class="email-masthead_name" href="{{ url('/') }}" target="_blank">
                            <img src="{{ asset("/img/logo.png") }}" class="logo-image" alt="Logo Image"
                                 style="max-width: 40px;"/>
                            &nbsp; &nbsp;
                            {{ config('app.name') }}
                        </a>
                    </td>
                </tr>

                <!-- Email Body -->
                <tr>
                    <td class="email-body" width="100%">
                        <table class="email-body_inner" align="center" width="570" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="{{ $fontFamily }}" class="email-body_cell">
                                    <!-- Greeting -->
                                    <h1 class="header-1">
                                        {{trans('messages.welcome')}},
                                    </h1>

                                    <p class="paragraph">{{trans('messages.yourCredentials')}}</p>

                                    <p class="paragraph">{{trans('messages.korisnickoIme')}}: {{$email}}</p>

                                    <p class="paragraph">{{trans('messages.password')}}: {{$password}}</p>

                                    <p class="paragraph paragraph-important">{{trans('messages.passwordPlsChange')}} </p>

                                    <!-- Action Button -->

                                    <table class="body_action" align="center" width="100%" cellpadding="0"
                                           cellspacing="0">
                                        <tr>
                                            <td align="center">
                                                <?php
                                                $actionColor = 'button--green';
                                                ?>

                                                <a href="{{ url('/') }}"
                                                   style="{{ $fontFamily }}"
                                                   class="button {{ $actionColor }}"
                                                   target="_blank">
                                                    {{trans('messages.logInButton')}}
                                                </a>
                                            </td>
                                        </tr>
                                    </table>


                                    <!-- Salutation -->
                                    <p class="paragraph">
                                        {{ config('app.name') }}
                                    </p>

                                    <!-- Sub Copy -->

                                    <table class="body_sub">
                                        <tr>
                                            <td style="{{ $fontFamily }}">
                                                <p class="paragraph-sub">
                                                    {{trans('messages.ifYouHaveTroubles',['action'=>trans('messages.logInButton')] ) }}
                                                </p>

                                                <p class="paragraph-sub">
                                                    <a class="anchor" href="{{ url('/') }}" target="_blank">
                                                        {{ url('/') }}
                                                    </a>
                                                </p>
                                            </td>
                                        </tr>
                                    </table>

                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <!-- Footer -->
                <tr>
                    <td>
                        <table class="email-footer" align="center" width="570" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="{{ $fontFamily }}" class="email-footer_cell">
                                    <p class="paragraph-sub-footer">
                                        &copy; {{ date('Y') }}
                                        <a class="anchor" href="{{ url('/') }}"
                                           target="_blank">{{ config('app.name') }}</a>.
                                        {{trans('messages.allRightsReserved')}}
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>