<tr>
    <td class="header">
        <a href="{{ $url }}">
            <img src="{{ asset("/img/logo.png") }}"  class="logo-image" alt="Logo Image" style="max-width: 40px;"/>
            &nbsp; &nbsp;

            {{ $slot }}
        </a>
    </td>
</tr>
