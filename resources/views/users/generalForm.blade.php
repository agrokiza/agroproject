@section('scripts')
    <script src="{{asset('js/users.js')}}"></script>
@endsection

<div class="form-group">
    <label for="name">{{ trans('messages.userName') }} *</label>
    {!! Form::text('name', null, array('class'=>'form-control', 'required'=>'', 'autofocus'=>'')) !!}
    @if ($errors->has('name'))
        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
    @endif
</div>

<div class="form-group">
    <label for="surname">{{ trans('messages.userSurname') }} *</label>
    {!! Form::text('surname', null, array('class'=>'form-control', 'required'=>'')) !!}
    @if ($errors->has('surname'))
        <span class="help-block">
                            <strong>{{ $errors->first('surname') }}</strong>
                        </span>
    @endif
</div>

<div class="form-group">
    <label for="streetandnumber">{{ trans('messages.userAddress') }} *</label>
    {!! Form::text('streetandnumber', null, array('class'=>'form-control', 'required'=>'')) !!}
    @if ($errors->has('streetandnumber'))
        <span class="help-block">
                            <strong>{{ $errors->first('streetandnumber') }}</strong>
                        </span>
    @endif
</div>

<div class="form-group">
    <label for="postalcode">{{ trans('messages.userZIP') }} *</label>
    {!! Form::text('postalcode', null, array('class'=>'form-control', 'required'=>'')) !!}
    @if ($errors->has('postalcode'))
        <span class="help-block">
                            <strong>{{ $errors->first('postalcode') }}</strong>
                        </span>
    @endif
</div>

<div class="form-group">
    <label for="city">{{ trans('messages.userCity') }} *</label>
    {!! Form::text('city', null, array('class'=>'form-control', 'required'=>'')) !!}
    @if ($errors->has('city'))
        <span class="help-block">
                            <strong>{{ $errors->first('city') }}</strong>
                        </span>
    @endif
</div>

<div class="form-group">
    <label for="country">{{ trans('messages.userCountry') }} *</label>
    {!! Form::text('country', null, array('class'=>'form-control', 'required'=>'')) !!}
    @if ($errors->has('country'))
        <span class="help-block">
                            <strong>{{ $errors->first('country') }}</strong>
                        </span>
    @endif
</div>

<div class="form-group">
    <label for="phonenumber">{{ trans('messages.userPhoneNumber') }} *</label>
    {!! Form::text('phonenumber', null, array('class'=>'form-control', 'required'=>'')) !!}
    @if ($errors->has('phonenumber'))
        <span class="help-block">
                            <strong>{{ $errors->first('phonenumber') }}</strong>
                        </span>
    @endif
</div>

<div class="form-group">
    <label for="subscription_id">{{ trans('messages.subscription') }} *</label>
    {!! Form::select('subscription_id', $subscriptions, null, array('class' => 'form-control input-sm', 'id'=>'subscription','required'=>'')) !!}
</div>

<div class="form-group">
    <label for="role">{{ trans('messages.role') }} *</label>
    {!! Form::select('role', $roles, null, array('class' => 'form-control input-sm', 'id'=>'role','required'=>'', 'onchange'=>'hideMqttSuper()')) !!}
</div>

<div class="form-group">
    <label for="locale">{{ trans('messages.language') }} *</label>
    {!! Form::select('locale', $locales, null, array('class' => 'form-control input-sm', 'id'=>'locale','required'=>'')) !!}
</div>

<div id="mqtt_super" class="checkbox">
    <label>
        {!! Form::checkbox('mqtt_super',1) !!} <b>{{trans('messages.mqttSuperuser')}}</b>
    </label>
</div>