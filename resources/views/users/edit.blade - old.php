@extends("template.template")

@section('headercss')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css">
@endsection

@section("content")
    <div class="box box-primary">
        <div class="box-header with-border">

        </div>

        {!! Form::model($user, ['accept-charset' => 'utf-8' ,
        'method' => 'PATCH', 'route' => ['users.update', $user->id]]) !!}

        {{csrf_field()}}

        <div class="box-body">
            <div class="form-group">
                <label for="name" class="control-label">{{trans('messages.userName')}} *</label>
                {!! Form::text('name', null, array('class'=>'form-control', 'required'=>'', 'autofocus'=>'')) !!}
                @if ($errors->has('name'))
                    <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                @endif
            </div>

            <div class="form-group">
                <label for="surname" class="control-label">{{trans('messages.userSurname')}} *</label>
                {!! Form::text('surname', null, array('class'=>'form-control', 'required'=>'')) !!}
                @if ($errors->has('surname'))
                    <span class="help-block">
                            <strong>{{ $errors->first('surname') }}</strong>
                        </span>
                @endif
            </div>

            <div class="form-group">
                <label for="streetandnumber" class="control-label">{{trans('messages.userAddress')}} *</label>
                {!! Form::text('streetandnumber', null, array('class'=>'form-control', 'required'=>'')) !!}
                @if ($errors->has('streetandnumber'))
                    <span class="help-block">
                            <strong>{{ $errors->first('streetandnumber') }}</strong>
                        </span>
                @endif
            </div>

            <div class="form-group">
                <label for="postalcode" class="control-label">{{trans('messages.userZIP')}} *</label>
                {!! Form::text('postalcode', null, array('class'=>'form-control', 'required'=>'')) !!}
                @if ($errors->has('postalcode'))
                    <span class="help-block">
                            <strong>{{ $errors->first('postalcode') }}</strong>
                        </span>
                @endif
            </div>

            <div class="form-group">
                <label for="city" class="control-label">{{trans('messages.userCity')}} *</label>
                {!! Form::text('city', null, array('class'=>'form-control', 'required'=>'')) !!}
                @if ($errors->has('city'))
                    <span class="help-block">
                            <strong>{{ $errors->first('city') }}</strong>
                        </span>
                @endif
            </div>

            <div class="form-group">
                <label for="country" class="control-label">{{trans('messages.userCountry')}} *</label>
                {!! Form::text('country', null, array('class'=>'form-control', 'required'=>'')) !!}
                @if ($errors->has('country'))
                    <span class="help-block">
                            <strong>{{ $errors->first('country') }}</strong>
                        </span>
                @endif
            </div>

            <div class="form-group">
                <label for="phonenumber" class="control-label">{{trans('messages.userPhoneNumber')}} *</label>
                {!! Form::text('phonenumber', null, array('class'=>'form-control', 'required'=>'')) !!}
                @if ($errors->has('phonenumber'))
                    <span class="help-block">
                            <strong>{{ $errors->first('phonenumber') }}</strong>
                        </span>
                @endif
            </div>

            <div class="form-group">
                <label for="role" class="control-label">{{trans('messages.role')}} *</label>
                {{--{!! Form::select('role',['user' => trans('messages.user'), 'admin' => trans('messages.admin')],null, array('class'=>'form-control','required'=>'', 'style' => 'max-width: 200px;')) !!}--}}
                {!! Form::select('role', $roles, null, array('class' => 'form-control input-sm', 'id'=>'role','required'=>'')) !!}
            </div>

            <div class="form-group">
                <label for="email" class="control-label">{{trans('messages.email')}}</label>
                {!! Form::email('email', null, array('class'=>'form-control', 'disabled'=>'')) !!}
                {{--@if ($errors->has('email'))
                    <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                @endif--}}
            </div>

        </div>
        <div class="box-footer">
            <a class="btn btn-primary btn-primary-my" role="button"
               data-toggle="modal" href="#myModalIzmena">
                {{ trans('messages.changeData') }}
            </a>
            <!-- Modal za izmenu -->
            <div id="myModalIzmena" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close"
                                    data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">{{ trans('messages.userChange') }}</h4>
                        </div>
                        <div class="modal-body">
                            <p>{{ trans('messages.userChangeAreUSure') }}</p>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success">{{trans('messages.yes')}}</button>
                            <a class="btn btn-danger" type="btn-link"
                               href="{{ route("users.all") }}">{{trans('messages.no')}}</a>
                        </div>
                    </div>

                </div>
            </div>
            <!-- Kraj modala za izmenu -->
            <a class="btn btn-danger" href="{{ URL::previous() }}">{{trans('messages.btnNazad')}}</a>
        </div>
        {!! Form::close() !!}
    </div>
@endsection