@extends('template.template')

@section('headercss')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('header')
    <link href="{{ asset("/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}" rel="stylesheet">
@stop


@section('scripts')
    <script src="{{ asset("/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}"></script>
    <script src="{{ asset("/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")}}"></script>

    <script src="{{ asset("/js/myalerts.js")}}"></script>

    <!-- FastClick -->
    <script src="{{ asset("/AdminLTE/plugins/fastclick/fastclick.js")}}"></script>

    <!-- page script -->
    <script>
        $(function () {
            $("#example1").DataTable(
                    {
                        //Iskljuci sortiranje kolona
                        "order": [6, 7, 8],
                        "columnDefs": [{
                            "targets": [6, 7, 8],
                            "orderable": false
                        }],

                        //Cuvanje podesavanja tabele
                        "stateSave": true,
                        "stateSaveParams": function (settings, data) {
                            data.search.search = "";
                            //console.log(data);
                        },

                        //Iskljuci inicijalno sortiranje
                        "language": {
                            // TODO: Promeniti jezik!
                            "url": "{{ asset(trans('messages.prevodDataTables'))}}",
                            "autoWidth": true
                        }
                    }
            );
        });

        function izmeniLink(id) {
            //console.log($('#dugmeZaBrisanje'));
            $('#dugmeZaBrisanje').attr("href", "/users/delete/" + id);
        }
    </script>

@stop

@section('content')

    <!-- Modal for deleting user -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">{{ trans('messages.dataDelete') }}</h4>
                </div>
                <div class="modal-body">
                    <p>{{ trans('messages.dataDeleteAreUSure') }}</p>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-success" href="" id="dugmeZaBrisanje">{{ trans('messages.yes') }}</a>
                    <button type="btn-link" class="btn btn-danger"
                            data-dismiss="modal">{{ trans('messages.no') }}</button>
                </div>
            </div>

        </div>
    </div>
    <!-- Kraj modala za brisanje -->

    <div class="box">

        <div class="box-body">

            <div class="row">

                <div class="col-sm-12">
                    <a class="btn btn-primary btn-primary-my"
                       href="/register">{{trans('messages.registrujkorisnika')}}</a>
                    </br>
                    </br>
                    <div class="table-responsive" style="min-height: 300px">
                        <table id="example1" class="table table-bordered table-striped dataTable" width="100%"
                               role="grid" aria-describedby="example1_info">
                            <thead>
                            <tr role="row">
                                <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    aria-sort="ascending"
                                    aria-label="{{trans('messages.clickToSort')}}">{{trans('messages.fullName')}}</th>

                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    aria-label="{{trans('messages.clickToSort')}}">{{trans('messages.city')}}</th>

                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    aria-label="{{trans('messages.clickToSort')}}">{{trans('messages.email')}}</th>

                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    aria-label="{{trans('messages.clickToSort')}}">{{trans('messages.subscription')}}</th>

                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    aria-label="{{trans('messages.clickToSort')}}">{{trans('messages.role')}}</th>

                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    aria-label="{{trans('messages.clickToSort')}}">{{trans('messages.active')}}</th>
                                <th class="no-sort">

                                </th>
                                <th class="no-sort">

                                </th>
                                <th class="no-sort">

                                </th>

                            </tr>
                            </thead>
                            <tbody>

                            <?php
                            $brojac = 1
                            ?>

                            @foreach($users as $user)
                                <tr class="{{ $brojac++%2 ? 'odd':'even' }}" role="row">

                                    {{-- <td style="min-width: 70px;">
                                         <div class="btn-group btn-group-xs">
                                             <button class="btn btn-primary btn-xs" data-toggle="dropdown"
                                                     type="button">{{trans('messages.options')}}</button>
                                             <button class="btn btn-primary dropdown-toggle btn-xs"
                                                     data-toggle="dropdown" type="button">
                                                 <span class="caret"></span>
                                                 <span class="sr-only">Toggle Dropdown</span>
                                             </button>
                                             <ul class="dropdown-menu" role="menu">
                                                 --}}{{--<li>
                                                     {!! link_to_route('korisnici.detalji', trans('messages.details'), array($korisnik->id)) !!}
                                                 </li>
                                                 <li>
                                                     @if($korisnik->isActive == true)
                                                         {!! link_to_route('korisnici.deaktiviraj', trans('messages.deactivate'), array($korisnik->id)) !!}
                                                     @else
                                                         {!! link_to_route('korisnici.aktiviraj', trans('messages.activate'), array($korisnik->id)) !!}
                                                     @endif
                                                 </li>
                                                 <li>
                                                     {!! link_to_route('korisnici.uredi', trans('messages.edit'), array($korisnik->id)) !!}
                                                 </li>
                                                 <li>
                                                     @php
                                                         //Proveri da li korisnik ima vezane podatke
                                                         $calcs = \App\Calculation::where('user_id', $korisnik->id)->count();

                                                     @endphp

                                                     @if($calcs==0)
                                                         {!! link_to_route('korisnici.obrisi', trans('messages.delete'), array($korisnik->id)) !!}
                                                     @endif


                                                 </li>--}}{{--
                                             </ul>
                                         </div>

                                     </td>--}}

                                    <td class="sorting_1">{{ $user->fullname }}</td>
                                    <td>{{ $user->city }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->subscription->name }}</td>
                                    <td>
                                        @if($user->role == 'user')
                                            {{trans('messages.user')}}
                                        @else
                                            {{trans('messages.admin')}}
                                        @endif
                                    </td>
                                    <td>
                                        @if($user->isActive == 0)
                                            {{trans('messages.no')}}
                                        @else
                                            {{trans('messages.yes')}}
                                        @endif
                                    </td>
                                    <td style="margin-left:0px; margin-top:0px; padding-left: 0px; padding-top: 0px; text-align: left">
                                        <a class="btn btn-link btn-link-MY" role="button"
                                           data-toggle="modal" href="#myModal"
                                           onclick="izmeniLink({{ $user->id }})">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </td>
                                    <td style="margin-left:0px; margin-top:0px; padding-left: 0px; padding-top: 0px; text-align: left">
                                        <a href="{{ route('users.edit', $user->id) }}"
                                           class="btn btn-link btn-link-MY" role="button">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </td>
                                    <td style="margin-left:0px; margin-top:0px; padding-left: 0px; padding-top: 0px; text-align: left">
                                        <a href="{{ route('users.activatedeactivate', $user->id) }}"
                                           class="btn btn-link btn-link-MY" role="button">
                                            <i class="fa fa-{{ $user->isActive == 1 ? 'toggle-on':'toggle-off' }}"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection