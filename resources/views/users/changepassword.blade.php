@extends('template.template')

@section('content')

    {!! Form::model($user, ['accept-charset' => 'utf-8' ,
     'method' => 'POST', 'route' => ['users.credentials.changepassword']]) !!}

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">{{trans('messages.userData')}}</h3>
        </div>

        <div class="box-body">
            {{ csrf_field() }}

            <div class="row" style="padding-bottom: 8px;">
                <div class="col-md-2"><b>{{trans('messages.fullName')}} </b></div>
                <div class="col-md-10">{{ $user->fullname }}</div>
            </div>

            <div class="row" style="padding-bottom: 8px;">
                <div class="col-md-2"><b>{{trans('messages.userAddress')}}</b></div>
                <div class="col-md-10">{{ $user->streetandnumber }}</div>
            </div>

            <div class="row" style="padding-bottom: 8px;">
                <div class="col-md-2"><b>{{trans('messages.userZIP')}}</b></div>
                <div class="col-md-10">{{ $user->postalcode }}</div>
            </div>

            <div class="row" style="padding-bottom: 8px;">
                <div class="col-md-2"><b>{{trans('messages.userCity')}}</b></div>
                <div class="col-md-10">{{ $user->city }}</div>
            </div>

            <div class="row" style="padding-bottom: 8px;">
                <div class="col-md-2"><b>{{trans('messages.userCountry')}}</b></div>
                <div class="col-md-10">{{ $user->country }}</div>
            </div>

            <div class="row" style="padding-bottom: 8px;">
                <div class="col-md-2"><b>{{trans('messages.userPhoneNumber')}}</b></div>
                <div class="col-md-10">{{ $user->phonenumber }}</div>
            </div>

            <div class="row" style="padding-bottom: 8px;">
                <div class="col-md-2"><b>{{trans('messages.subscription')}}</b></div>
                <div class="col-md-10">{{ $user->subscription->name }}</div>
            </div>

            <div class="row" style="padding-bottom: 8px;">
                <div class="col-md-2"><b>{{trans('messages.role')}}</b></div>
                <div class="col-md-10">
                    @if($user->role == 'user')
                        {{trans('messages.user')}}
                    @else
                        {{trans('messages.admin')}}
                    @endif
                </div>
            </div>

            <div class="row" style="padding-bottom: 8px;">
                <div class="col-md-2"><b>{{trans('messages.email')}}</b></div>
                <div class="col-md-9">{{ $user->email }}</div>
            </div>

            <div class="box-footer">
            </div>

        </div>
    </div>

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">{{trans('messages.changePassword')}}</h3>
        </div>

        <div class="box-body">
            {{ csrf_field() }}

            <div class="form-group has-feedback">
                <label>{{trans('messages.oldPassword')}}</label>
                <input id="oldpassword" name="oldpassword" type="password" class="form-control" required>

                @if ($errors->has('oldpassword'))
                    <span class="help-block">
                        <strong>{{ $errors->first('oldpassword') }}</strong>
                    </span>
                @endif

            </div>

            <div class="form-group has-feedback">
                <label>{{trans('messages.newPassword')}}</label>
                <input id="password" name="password" type="password" class="form-control" required>

                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group has-feedback">
                <label>{{trans('messages.confirmNewPassword')}}</label>
                <input id="password-confirm" name="password_confirmation" type="password" class="form-control" required>

                @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                @endif
            </div>


            <div class="box-footer">
                <button class="btn btn-primary" type="submit">{{trans('messages.saveChanges')}}</button>
                <a class="btn btn-danger" href="{{ URL::previous() }}">{{trans('messages.btnNazad')}}</a>
            </div>

        </div>
    </div>
    {!! Form::close() !!}
@endsection