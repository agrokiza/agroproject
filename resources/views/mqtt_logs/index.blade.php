@extends('template.template')

@section('headercss')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('header')
    <link href="{{ asset("/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}" rel="stylesheet">
@stop


@section('scripts')
    <script src="{{ asset("/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}"></script>
    <script src="{{ asset("/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")}}"></script>

    <!-- FastClick -->
    <script src="{{ asset("/AdminLTE/plugins/fastclick/fastclick.js")}}"></script>

    <script>
        var jezik = "{{ asset(trans('messages.prevodDataTables'))}}";
    </script>

    <!-- page script -->
    <script src="{{asset("/js/mqtt_logs.js")}}"></script>

@stop

@section('content')

    <div class="box">

        <div class="box-body">

            <div class="row">

                <div class="col-sm-12">
                    <div class="form-group">
                        <label>{{trans('messages.numberOfRecords')}}</label>
                        <select id="last" onchange="osveziTabelu()" class="form-control">
                            <option value="1000" selected>1.000</option>
                            <option value="10000">10.000</option>
                            <option value="100000">100.000</option>
                            <option value="1000000">1.000.000</option>
                        </select>
                    </div>
                    <div class="table-responsive" style="min-height: 300px">
                        <table id="logs_table" class="table table-bordered table-striped dataTable" width="100%"
                               role="grid" aria-describedby="example1_info">
                            <thead>
                            <tr role="row">

                                <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    aria-sort="ascending"
                                    aria-label="{{trans('messages.clickToSort')}}">{{trans('messages.message')}}
                                </th>

                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    aria-label="{{trans('messages.clickToSort')}}">{{trans('messages.receivedAt')}}
                                </th>

                            </tr>
                            </thead>

                            <tbody>


                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection