<div class="form-group">
    <label for="description">{{trans('messages.description')}} *</label>
    {!! Form::text('description', null, array('class'=>'form-control', 'required' => '', 'autofocus'=>'')) !!}
    @if ($errors->has('description'))
        <span class="help-block">
                            <strong>{{ $errors->first('description') }}</strong>
                        </span>
    @endif
</div>

<div class="checkbox">
    <label>{!! Form::checkbox('is_active') !!}
        <small><b>{{trans('messages.activeRule')}}</b></small>
    </label>
</div>


<div class="form-group">
    <label for="device_id">{{trans('messages.device')}} *</label>
    {!! Form::select('device_id', $devices,null, array('class'=>'form-control','placeholder'=>'', 'required' => '')) !!}
    @if ($errors->has('device_id'))
        <span class="help-block">
                            <strong>{{ $errors->first('device_id') }}</strong>
                        </span>
    @endif
</div>


<div class="row">
    <div class="col-lg-12">
        <div id="containerConditionsMaster" style="display: none;">
            <h3 style="background-color: #9BC53D; text-align: center;">{{trans('messages.conditions')}}</h3>
            <div id="containerConditions">

                @if(!empty(old('conditions')))
                    @foreach(old('conditions') as $condition)
                @section('scripts')
                    @parent
                    <script>
                        Lang.setLocale('{{Cookie::get('locale')}}');
                        AddCondition({!! json_encode($condition) !!})
                    </script>
                @endsection
                @endforeach
                @else
                    @if(!empty($conditions))
                        @foreach($conditions as $condition)
                @section('scripts')
                    @parent
                    <script>
                        Lang.setLocale('{{Cookie::get('locale')}}');
                        AddCondition({!! json_encode($condition) !!})
                    </script>
                @endsection
                @endforeach

                @endif
                @endif
            </div>
            <button type="button" class="btn btn-facebook"
                    onclick="AddCondition()">{{trans('messages.addCondition')}}</button>
        </div>
    </div>
    <div class="col-lg-12">
        <div id="containerActionsMaster" style="display: none;">
            <h3 style="background-color: #FEF237; text-align: center;">{{trans('messages.actions')}}</h3>
            <div id="containerActions">

                @if(!empty(old('actions')))
                    @foreach(old('actions') as $action)
                @section('scripts')
                    @parent
                    <script>
                        Lang.setLocale('{{Cookie::get('locale')}}');
                        AddAction({!! json_encode($action) !!})
                    </script>
                @endsection
                @endforeach
                @else
                    @if(!empty($actions))
                        @foreach($actions as $action)
                @section('scripts')
                    @parent
                    <script>
                        Lang.setLocale('{{Cookie::get('locale')}}');
                        AddAction({!! json_encode($action) !!})
                    </script>
                @endsection
                @endforeach
                @endif
                @endif
            </div>
            <button type="button" class="btn btn-facebook"
                    onclick="AddAction()">{{trans('messages.addAction')}}</button>
        </div>
    </div>
</div>

@section('scripts')
    <script src="{{asset('js/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script src="{{asset('js/jquery-validation/dist/additional-methods.js')}}"></script>
    <script src="{{ asset ("/js/jquery-validation/dist/localization/messages_".Cookie::get('locale').".js") }}"></script>

    <script src="{{asset('js/common-functions.js')}}"></script>
    <script src="{{asset('js/automation.js')}}"></script>
@endsection