@extends('template.template')

@section('headercss')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('content')
    <div class='row'>
        @if(Auth::user()->role=='admin')

        @else
            @include('welcome.usercontrolpanel')
        @endif
    </div><!-- /.row -->
@endsection