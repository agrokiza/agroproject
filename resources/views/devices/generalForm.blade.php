<div class="form-group">
    <label for="user_id">{{trans('messages.user')}} *</label>
    {!! Form::select('user_id', $users,null, array('class'=>'form-control','placeholder'=>'', 'required' => '', 'autofocus'=>'')) !!}
    @if ($errors->has('user_id'))
        <span class="help-block">
                            <strong>{{ $errors->first('user_id') }}</strong>
                        </span>
    @endif
</div>

<div class="form-group">
    <label for="description">{{trans('messages.description')}}</label>
    {!! Form::text('description', null, array('class'=>'form-control')) !!}
    @if ($errors->has('description'))
        <span class="help-block">
                            <strong>{{ $errors->first('description') }}</strong>
                        </span>
    @endif
</div>

<div class="form-group">
    <label for="location">{{trans('messages.location')}}</label>
    {!! Form::text('location', null, array('class'=>'form-control')) !!}
    @if ($errors->has('location'))
        <span class="help-block">
                            <strong>{{ $errors->first('location') }}</strong>
                        </span>
    @endif
</div>

<div class="form-group">
    <label for="mac">{{trans('messages.macAddress')}}</label>
    {!! Form::text('mac', null, array('class'=>'form-control')) !!}
    @if ($errors->has('mac'))
        <span class="help-block">
                            <strong>{{ $errors->first('mac') }}</strong>
                        </span>
    @endif
</div>

<div class="form-group">
    <label for="ip">{{trans('messages.ipAddress')}}</label>
    {!! Form::text('ip', null, array('class'=>'form-control')) !!}
    @if ($errors->has('ip'))
        <span class="help-block">
                            <strong>{{ $errors->first('ip') }}</strong>
                        </span>
    @endif
</div>

<h3>{{trans('messages.inputs')}}</h3>
<hr>
<div id="containerInputs">

    @if(Request::is('devices/edit*'))
        @foreach($inputs as $input)
    @section('scripts')
        @parent
        <script>
            AddInput({
                id: '{{$input->id}}',
                description: '{{$input->description}}',
                unit_id: '{{$input->unit_id}}',
                min: '{{$input->min}}',
                max: '{{$input->max}}'
            })
        </script>
    @endsection
    @endforeach
    @endif

</div>
<button type="button" class="btn btn-facebook" onclick="AddInput()">{{trans('messages.addInput')}}</button>


<h3>{{trans('messages.outputs')}}</h3>
<hr>
<div id="containerOutputs">

    @if(Request::is('devices/edit*'))
        @foreach($outputs as $output)
    @section('scripts')
        @parent
        <script>
            AddOutput({
                id: '{{$output->id}}',
                description: '{{$output->description}}',
                type_id: '{{$output->type_id}}'
            })
        </script>
    @endsection
    @endforeach
    @endif

</div>
<button type="button" class="btn btn-facebook" onclick="AddOutput()">{{trans('messages.addOutput')}}</button>

@section('scripts')
    <script>
        var otCombo = '{!! Form::select('type_id', $ot,null, array('id'=>'idtypeid', 'class'=>'form-control','placeholder'=>trans('messages.choseOutputType'), 'required' => '')) !!}';
        var iuCombo = '{!! Form::select('unit_id', $iu,null, array('id'=>'idunitid', 'class'=>'form-control','placeholder'=>trans('messages.choseInputUnit'), 'required' => '')) !!}';
        var labelDescription = '{{trans('messages.description')}}';
        var labelMin = '{{trans('messages.min')}}';
        var labelMax = '{{trans('messages.max')}}';
    </script>
    <script src="{{asset('js/devices.js')}}"></script>
@endsection
