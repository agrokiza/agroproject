@extends("template.template")

@section('headercss')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('AdminLTE/plugins/colorpicker/bootstrap-colorpicker.min.css') }}" rel="stylesheet"
          type="text/css">
@endsection

@section("content")
    <div class="box box-primary">
        <div class="box-header with-border">

        </div>

        {!! Form::model($device, ['accept-charset' => 'utf-8' ,
        'method' => 'PATCH', 'route' => ['devices.customizePost', $device->id]]) !!}

        {{csrf_field()}}

        <div class="box-body">

            <div class="form-group">
                <label for="description">{{trans('messages.description')}}</label>
                {!! Form::text('description', null, array('class'=>'form-control')) !!}
                @if ($errors->has('description'))
                    <span class="help-block">
                            <strong>{{ $errors->first('description') }}</strong>
                        </span>
                @endif
            </div>

            <div class="form-group">
                <label for="location">{{trans('messages.location')}}</label>
                {!! Form::text('location', null, array('class'=>'form-control')) !!}
                @if ($errors->has('location'))
                    <span class="help-block">
                            <strong>{{ $errors->first('location') }}</strong>
                        </span>
                @endif
            </div>

            @if(count($inputs)>0)
                <h3>{{trans('messages.inputs')}}</h3>
                <hr>
                <div id="containerInputs">

                    @foreach($inputs as $input)
                        <div class="row">
                            <input type="hidden" value="{{$input->id}}" name="inputs[{{$input->id}}][id]">

                            <div class="col-lg-3">
                                <label>
                                    <small>{{trans('messages.description')}}</small>
                                </label>
                                <input class="form-control" type="text" name="inputs[{{$input->id}}][description]"
                                       placeholder="{{trans('messages.description')}}" value="{{$input->description}}"
                                       required>
                            </div>


                            <div class="col-lg-1"{{-- style="display: {{$input->unit_id==6? 'none':'block'}};"--}}>
                                <label>
                                    <small>{{trans('messages.min')}}</small>
                                </label>
                                <input class="form-control" type="number" step="0.01"
                                       name="inputs[{{$input->id}}][min]"
                                       placeholder="{{trans('messages.min')}}" value="{{$input->min}}">
                            </div>

                            <div class="col-lg-1"{{-- style="display: {{$input->unit_id==6? 'none':'block'}};"--}}>
                                <label>
                                    <small>{{trans('messages.max')}}</small>
                                </label>
                                <input class="form-control" type="number" step="0.01"
                                       name="inputs[{{$input->id}}][max]" placeholder="{{trans('messages.max')}}"
                                       value="{{$input->max}}">
                            </div>


                            <div class="col-lg-2">
                                <label>
                                    <small>{{trans('messages.bgColor')}}</small>
                                </label>
                                <div id="colorPicker{{$input->id}}" class="input-group colorpicker-component">
                                    <input class="form-control" type="text"
                                           name="inputs[{{$input->id}}][ui_bg_color]"
                                           placeholder="{{trans('messages.bgColor')}}"
                                           value="{{$input->ui_bg_color}}">
                                    <span class="input-group-addon"><i></i></span>
                                </div>

                            </div>

                            @if($input->unit_id==6)
                                <div class="col-lg-2">
                                    <label>
                                        <small>{{trans('messages.indColor')}}</small>
                                    </label>
                                    <div id="colorPickerInd{{$input->id}}" class="input-group colorpicker-component">
                                        <input class="form-control" type="text"
                                               name="inputs[{{$input->id}}][ui_ind_color]"
                                               placeholder="{{trans('messages.indColor')}}"
                                               value="{{$input->ui_ind_color}}">
                                        <span class="input-group-addon"><i></i></span>
                                    </div>

                                </div>
                            @endif

                            {{--@if($input->unit_id!=6)--}}
                                <div class="col-lg-2">
                                    <div class="checkbox" style="padding-top: 15px;">
                                        <label><input type="checkbox"
                                                      name="inputs[{{$input->id}}][notify_me]" {{$input->notify_me==1? 'checked':''}}>
                                            <small><b>{{trans('messages.notifyMe')}}</b></small>
                                        </label>
                                    </div>
                                </div>
                            {{--@endif--}}

                        </div>
                        <hr>
                    @endforeach

                </div>
            @endif

            @if(count($outputs)>0)
                <h3>{{trans('messages.outputs')}}</h3>
                <hr>
                <div id="containerOutputs">

                    @foreach($outputs as $output)
                        <div class="row">
                            <input type="hidden" value="{{$output->id}}" name="outputs[{{$output->id}}][id]">

                            <div class="col-lg-3">
                                <label>
                                    <small>{{trans('messages.description')}}</small>
                                </label>
                                <input class="form-control" type="text" name="outputs[{{$output->id}}][description]"
                                       placeholder="{{trans('messages.description')}}" value="{{$output->description}}"
                                       required>
                            </div>

                            <div class="col-lg-2">
                                <label>
                                    <small>{{trans('messages.bgColor')}}</small>
                                </label>
                                <div id="colorPickerOut{{$output->id}}" class="input-group colorpicker-component">
                                    <input class="form-control" type="text"
                                           name="outputs[{{$output->id}}][ui_bg_color]"
                                           placeholder="{{trans('messages.bgColor')}}"
                                           value="{{$output->ui_bg_color}}">
                                    <span class="input-group-addon"><i></i></span>
                                </div>

                            </div>

                        </div>
                        <hr>
                    @endforeach

                </div>
            @endif


        </div>
        <div class="box-footer">
            <a class="btn btn-primary btn-primary-my" role="button"
               data-toggle="modal" href="#myModalIzmena">
                {{ trans('messages.changeData') }}
            </a>
            <!-- Modal za izmenu -->
            <div id="myModalIzmena" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close"
                                    data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">{{trans('messages.dataChange')}}</h4>
                        </div>
                        <div class="modal-body">
                            <p>{{trans('messages.dataChangeAreUSure')}}</p>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success">{{trans('messages.yes')}}</button>
                            <button type="button" class="btn btn-danger"
                                    data-dismiss="modal">{{trans('messages.no')}}</button>
                        </div>
                    </div>

                </div>
            </div>
            <!-- Kraj modala za izmenu -->
            <a class="btn btn-danger" href="{{ URL::previous() }}">{{trans('messages.btnNazad')}}</a>
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('scripts')
    <script src="{{asset('AdminLTE/plugins/colorpicker/bootstrap-colorpicker.min.js')}}"></script>

    <script>
        $(function () {
            $('[id^=colorPicker]').colorpicker({
                format: "hex"
            });
        });
    </script>
@endsection