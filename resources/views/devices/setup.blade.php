@extends("template.template")

@section('headercss')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css">
@endsection

@section("content")
    <div class="box box-primary">
        <div class="box-header with-border">

        </div>

        <div class="box-body">
            <h3>{{trans('messages.udi')}}</h3>
            <hr>

            <b>{{$device->description}}:&nbsp;</b>{{$device->udi}}
            <hr>


            <h3>{{trans('messages.inputs')}}</h3>
            <hr>
            @foreach($inputs as $input)
                <b>{{$input->description}}:&nbsp;</b>{{$input->uui}}
                <hr>
            @endforeach

            <h3>{{trans('messages.outputs')}}</h3>
            <hr>
            @foreach($outputs as $output)
                <b>{{$output->description}}:&nbsp;</b>{{$output->uui}}
                <hr>
            @endforeach
        </div>

        <div class="box-footer">

            <!-- Kraj modala za izmenu -->

            <a class="btn btn-danger" href="{{ URL::previous() }}">{{trans('messages.btnNazad')}}</a>
        </div>
        {!! Form::close() !!}
    </div>
@endsection


