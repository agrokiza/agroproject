@extends("template.template")

@section('headercss')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css">
@endsection

@section("content")
    <div class="box box-primary">
        <div class="box-header with-border">

        </div>

        {!! Form::model($device, ['accept-charset' => 'utf-8' ,
        'method' => 'PATCH', 'route' => ['devices.update', $device->id]]) !!}

        {{csrf_field()}}

        <div class="box-body">

            @include('devices.generalForm')

        </div>
        <div class="box-footer">
            <a class="btn btn-primary btn-primary-my" role="button"
               data-toggle="modal" href="#myModalIzmena">
                {{ trans('messages.changeData') }}
            </a>
            <!-- Modal za izmenu -->
            <div id="myModalIzmena" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close"
                                    data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">{{trans('messages.dataChange')}}</h4>
                        </div>
                        <div class="modal-body">
                            <p>{{trans('messages.dataChangeAreUSure')}}</p>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success">{{trans('messages.yes')}}</button>
                            <button type="button" class="btn btn-danger"
                                    data-dismiss="modal">{{trans('messages.no')}}</button>
                        </div>
                    </div>

                </div>
            </div>
            <!-- Kraj modala za izmenu -->
            <a class="btn btn-danger" href="{{ URL::previous() }}">{{trans('messages.btnNazad')}}</a>
        </div>
        {!! Form::close() !!}
    </div>
@endsection