@extends('template.template')

@section('headercss')
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('header')
    <link href="{{ asset("/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}" rel="stylesheet">
@stop

@section('scripts')
    <script src="{{ asset("/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}"></script>
    <script src="{{ asset("/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")}}"></script>

    <!-- FastClick -->
    <script src="{{ asset("/AdminLTE/plugins/fastclick/fastclick.js")}}"></script>

    <script>
        var jezik = "{{ asset(trans('messages.prevodDataTables'))}}";
        var deleteError = "{{trans('messages.dataDeleteError')}}";
    </script>

    <!-- page script -->
    <script src="{{asset("/js/devices_index.js")}}"></script>
@stop

@section('content')

    <!-- Modal za brisanje korisnika -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">{{trans('messages.dataDelete')}}</h4>
                </div>
                <div class="modal-body">
                    <p>{{trans('messages.dataDeleteAreUSure')}}</p>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success" onclick="obrisi()"
                            id="dugmeZaBrisanje">{{ trans('messages.yes') }}</button>
                    <button type="btn-link" class="btn btn-danger"
                            data-dismiss="modal">{{ trans('messages.no') }}</button>
                </div>
            </div>

        </div>
    </div>
    <!-- Kraj modala za brisanje -->

    <div class="box">

        <div class="box-body">

            <div class="row">

                <div class="col-sm-12">
                    <a class="btn btn-primary btn-primary-my"
                       href="{{route('devices.add')}}">{{trans('messages.addDevice')}}</a>
                    </br>
                    </br>
                    <div class="table-responsive" style="min-height: 300px">
                        <table id="devices_table" class="table table-bordered table-striped dataTable" width="100%"
                               role="grid" aria-describedby="example1_info">
                            <thead>
                            <tr role="row">

                                <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    aria-sort="ascending"
                                    aria-label="{{trans('messages.clickToSort')}}">{{trans('messages.user')}}
                                </th>

                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    aria-label="{{trans('messages.clickToSort')}}">{{trans('messages.udi')}}
                                </th>

                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    aria-label="{{trans('messages.clickToSort')}}">{{trans('messages.description')}}
                                </th>

                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    aria-label="{{trans('messages.clickToSort')}}">{{trans('messages.lastActivity')}}
                                </th>

                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    aria-label="{{trans('messages.clickToSort')}}">{{trans('messages.active')}}
                                </th>

                                <th class="no-sort"></th>

                                <th class="no-sort"></th>

                                <th class="no-sort"></th>

                            </tr>
                            </thead>
                            <tbody>


                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection