<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>{{config('app.name', 'Laravel')}}</title>

    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
    <!-- Tell the browser to be responsive to screen width -->

    <link rel="shortcut icon" href="{{asset("favicon.ico")}}"/>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="{{ asset("AdminLTE/bootstrap/css/bootstrap.min.css") }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset("AdminLTE/dist/css/AdminLTE.min.css")}}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ asset("AdminLTE/plugins/iCheck/square/blue.css")}}">


    <!-- jQuery 2.2.3 -->
    <script src="https://code.jquery.com/jquery-2.2.3.min.js"
            integrity="sha256-a23g1Nt4dtEYOj7bR+vTu7+T8VP13humZFBJNIYoEJo="
            crossorigin="anonymous"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="{{ asset ("AdminLTE/bootstrap/js/bootstrap.min.js") }}"></script>
    <!-- iCheck -->
    <script src="{{ asset ("AdminLTE/plugins/iCheck/icheck.min.js") }}"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Scripts -->

    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>


    <script>
        window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
        ]); ?>
    </script>

    <style>
        .btn-group.open .dropdown-toggle {
            -webkit-box-shadow: inset 0 0px 0px rgba(0,0,0,0);
            box-shadow: inset 0 0px 0px rgba(0,0,0,0);
        }

        .btn-moj .btn {
            padding-top: 0px;
            background-color: #ffffff;
            box-shadow: none;
            color: #9BC53D;
        }
    </style>

</head>
<body class="hold-transition login-page">

<div class="login-box">

    <noscript>

        <div class="callout callout-danger">
            {{ trans('messages.JSWarning') }}
        </div>
    </noscript>

    <div class="login-logo">
        <a href="/" style="color: black; font-size: 30px;"> <img src="{{ asset("/img/logo.png") }}" class="logo-image"
                                                                 alt="Logo Image" style="max-width: 100px;"/>
            <br><b>{{config('app.name')}}</b></a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">

        @if (Session::has('message'))
        </br>
        <div class="callout callout-warning">

            {{ Session::get('message') }}

        </div>
        @endif

        <p class="login-box-msg">{{trans('messages.pleaseLogIn')}}</p>

        <form role="form" method="POST" action="{{ url('/login') }}">
            {{ csrf_field() }}
            <div class="form-group has-feedback">
                <input id="email" name="email" type="email" class="form-control"
                       placeholder="{{trans('messages.email')}}" required>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group has-feedback">
                <input id="password" name="password" type="password" class="form-control"
                       placeholder="{{trans('messages.password')}}" required>

                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
                @if (isset($error))
                    <span class="help-block">
                        <strong>{!! $error !!}</strong>
                    </span>
                @endif

            </div>
            <div class="row">
                <div class="col-xs-7">
                    <div class="checkbox icheck">
                        <label>
                            <input type="checkbox" name="remember"> {{trans('messages.rememberMe')}}
                        </label>
                    </div>
                </div>

                <!-- /.col -->
                <div class="col-xs-5">
                    <button type="submit"
                            class="btn btn-primary btn-primary-my btn-block btn-flat">{{trans('messages.logInButton')}}</button>
                </div>
                <!-- /.col -->
            </div>

        </form>

        <!-- /.social-auth-links -->

        <a href="{{ url('/password/reset') }}">{{trans('messages.forgotPassword')}}</a>

        <div class="btn-group btn-moj pull-right">
            <button type="button" class="btn btn-moj dropdown-toggle" data-toggle="dropdown">{{trans('messages.language')}}</button>
            <button type="button" class="btn btn-moj dropdown-toggle" data-toggle="dropdown">
                <span class="caret"></span>
                <span class="sr-only">Toggle Dropdown</span>
            </button>
            <ul class="dropdown-menu" role="menu">
                <li><a href="{{url('/setlocale/de')}}">{{trans('messages.deutsch')}}</a></li>
                <li><a href="{{url('/setlocale/sr')}}">{{trans('messages.serbian')}}</a></li>
                <li><a href="{{url('/setlocale/en')}}">{{trans('messages.english')}}</a></li>
            </ul>
        </div>

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
</body>
</html>
