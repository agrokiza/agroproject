@extends('template.template')

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">

        </div>
        {!! Form::open(['method' => 'POST', 'url' => '/register']) !!}
        {{ csrf_field() }}

            <div class="box-body">
                <div class="form-group">
                    <label for="name">{{ trans('messages.userName') }} *</label>
                    {!! Form::text('name', null, array('class'=>'form-control', 'required'=>'', 'autofocus'=>'')) !!}
                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="surname">{{ trans('messages.userSurname') }} *</label>
                    {!! Form::text('surname', null, array('class'=>'form-control', 'required'=>'')) !!}
                    @if ($errors->has('surname'))
                        <span class="help-block">
                            <strong>{{ $errors->first('surname') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="streetandnumber">{{ trans('messages.userAddress') }} *</label>
                    {!! Form::text('streetandnumber', null, array('class'=>'form-control', 'required'=>'')) !!}
                    @if ($errors->has('streetandnumber'))
                        <span class="help-block">
                            <strong>{{ $errors->first('streetandnumber') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="postalcode">{{ trans('messages.userZIP') }} *</label>
                    {!! Form::text('postalcode', null, array('class'=>'form-control', 'required'=>'')) !!}
                    @if ($errors->has('postalcode'))
                        <span class="help-block">
                            <strong>{{ $errors->first('postalcode') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="city">{{ trans('messages.userCity') }} *</label>
                    {!! Form::text('city', null, array('class'=>'form-control', 'required'=>'')) !!}
                    @if ($errors->has('city'))
                        <span class="help-block">
                            <strong>{{ $errors->first('city') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="country">{{ trans('messages.userCountry') }} *</label>
                    {!! Form::text('country', null, array('class'=>'form-control', 'required'=>'')) !!}
                    @if ($errors->has('country'))
                        <span class="help-block">
                            <strong>{{ $errors->first('country') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="phonenumber">{{ trans('messages.userPhoneNumber') }} *</label>
                    {!! Form::text('phonenumber', null, array('class'=>'form-control', 'required'=>'')) !!}
                    @if ($errors->has('phonenumber'))
                        <span class="help-block">
                            <strong>{{ $errors->first('phonenumber') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="role">{{ trans('messages.role') }} *</label>
                    {{--{!! Form::select('role',['user' => trans('messages.user'), 'admin' => trans('messages.admin')], null, array('class'=>'form-control','required'=>'', 'style' => 'max-width: 200px;')) !!}--}}
                    {!! Form::select('role', $roles, null, array('class' => 'form-control input-sm', 'id'=>'role','required'=>'')) !!}
                </div>

                <div class="form-group">
                    <label for="email">{{ trans('messages.email') }} *</label>
                    {!! Form::email('email', null, array('class'=>'form-control', 'required'=>'')) !!}
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="password">{{ trans('messages.password') }} *</label>
                    {!! Form::password('password', array('class'=>'form-control', 'required'=>'')) !!}
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="password-confirm">{{ trans('messages.passwordConfirm') }} *</label>
                    {!! Form::password('password_confirmation', array('class'=>'form-control', 'required'=>'')) !!}
                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group" style="display: none;">
                    <button class="btn btn-primary btn-primary-my" type="button"
                            onclick="RandomPass()">{{trans('messages.passwordRandom')}}</button>
                </div>

                <script>
                    function RandomPass() {
                        var rString = randomString(6, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
                        $('#password').val(rString);
                        $('#password-confirm').val(rString);
                    }

                    function randomString(length, chars) {
                        var result = '';
                        for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
                        return result;
                    }
                </script>


            </div>
            <div class="box-footer">
                <button class="btn btn-primary btn-primary-my" type="submit">{{trans('messages.btnRegister')}}</button>
                <a class="btn btn-danger" href="{{ URL::previous() }}">{{trans('messages.btnNazad')}}</a>
            </div>
        {!! Form::close() !!}
    </div>

@endsection