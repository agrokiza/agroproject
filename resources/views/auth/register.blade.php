@extends('template.template')

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">

        </div>
        {!! Form::open(['method' => 'POST', 'url' => '/register']) !!}
        {{ csrf_field() }}

            <div class="box-body">

                @include('users.generalForm')

                <div class="form-group">
                    <label for="email">{{ trans('messages.email') }} *</label>
                    {!! Form::email('email', null, array('class'=>'form-control', 'required'=>'')) !!}
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="password">{{ trans('messages.password') }} *</label>
                    {!! Form::password('password', array('class'=>'form-control', 'required'=>'')) !!}
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="password-confirm">{{ trans('messages.passwordConfirm') }} *</label>
                    {!! Form::password('password_confirmation', array('class'=>'form-control', 'required'=>'')) !!}
                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group" style="display: none;">
                    <button class="btn btn-primary btn-primary-my" type="button"
                            onclick="RandomPass()">{{trans('messages.passwordRandom')}}</button>
                </div>

                <script>
                    function RandomPass() {
                        var rString = randomString(6, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
                        $('#password').val(rString);
                        $('#password-confirm').val(rString);
                    }

                    function randomString(length, chars) {
                        var result = '';
                        for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
                        return result;
                    }
                </script>


            </div>
            <div class="box-footer">
                <button class="btn btn-primary btn-primary-my" type="submit">{{trans('messages.btnRegister')}}</button>
                <a class="btn btn-danger" href="{{ URL::previous() }}">{{trans('messages.btnNazad')}}</a>
            </div>
        {!! Form::close() !!}
    </div>

@endsection