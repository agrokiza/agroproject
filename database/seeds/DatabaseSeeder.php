<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $result = \App\User::where('email', 'belteksllc@gmail.com')->get();
        if (count($result) == 0) {
            DB::table('users')->insert([
                'name' => 'Zoran',
                'surname' => 'Stankovic',
                'streetandnumber' => '1',
                'postalcode' => '11000',
                'city' => 'Beograd',
                'phonenumber' => '333',
                'email' => 'belteksllc@gmail.com',
                'password' => bcrypt('tajna'),
                'pw' => \App\Helpers\MqttPw::mqtt_pw('VacJGvSSumPdFqPA'),
                'mqtt_super' => 1,
                'role' => 'admin'
            ]);
        }

        $result = \App\User::where('email', 'reader@stronganic.rs')->get();
        if (count($result) == 0) {
            DB::table('users')->insert([
                'name' => 'Reader',
                'surname' => 'Universal',
                'streetandnumber' => '1',
                'postalcode' => '11000',
                'city' => 'Beograd',
                'phonenumber' => '333',
                'email' => 'reader@stronganic.rs',
                'password' => bcrypt('tWeqkop2Bn*'),
                'pw' => \App\Helpers\MqttPw::mqtt_pw('tWeqkop2Bn*'),
                'mqtt_super' => 0,
                'role' => 'user'
            ]);
        }

        $result = \App\Acl::where('email', 'reader@stronganic.rs')->where('topic', '#')->where('rw', 1)->get();
        if (count($result) == 0) {
            DB::table('acls')->insert([
                'email' => 'reader@stronganic.rs',
                'topic' => '#',
                'rw' => 1
            ]);
        }

        $result = \App\Subscription::where('id', 1)->get();
        if (count($result) == 0) {
            DB::table('subscriptions')->insert([
                'id' => 1,
                'name' => 'Silver',
                'features' => '[{"email_notifications":true,"sms_notifications":false,"push_notifications":true,"max_devices":5,"email_support":true,"phone_support":false}]'
            ]);
        }

        $result = \App\Subscription::where('id', 2)->get();
        if (count($result) == 0) {
            DB::table('subscriptions')->insert([
                'id' => 2,
                'name' => 'Gold',
                'features' => '[{"email_notifications":true,"sms_notifications":true,"push_notifications":true,"max_devices":10,"email_support":true,"phone_support":false}]'
            ]);
        }

        $result = \App\Subscription::where('id', 3)->get();
        if (count($result) == 0) {
            DB::table('subscriptions')->insert([
                'id' => 3,
                'name' => 'Platinum',
                'features' => '[{"email_notifications":true,"sms_notifications":true,"push_notifications":true,"max_devices":20,"email_support":true,"phone_support":true}]'
            ]);
        }
    }
}
