<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInputEvents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('input_events', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('input_id');
            $table->string('type',20)->nullable();
            $table->double('set')->nullable();
            $table->double('read')->nullable();
            $table->boolean('notify_me')->default(0);
            $table->boolean('is_notified')->default(0);
            $table->boolean('is_active')->default(1);
            $table->timestamps();
            $table->index('input_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('input_events');
    }
}
