<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMqttMessagesHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mqtt_messages_history', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('device_id');
            $table->integer('unit_id');
            $table->char('type',1);
            $table->text('message');
            $table->boolean('enable')->default(0);
            $table->index(['device_id', 'unit_id', 'type']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mqtt_messages_history');
    }
}
