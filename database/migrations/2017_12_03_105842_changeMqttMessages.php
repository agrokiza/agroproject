<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeMqttMessages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mqtt_messages', function (Blueprint $table) {
            //
            $table->dropColumn(['email', 'updated_at', 'topic']);
            $table->text('message')->change();
            $table->integer('device_id')->after('id');
            $table->integer('unit_id')->after('device_id');
            $table->char('type', 1)->after('unit_id');
            $table->index(['device_id', 'unit_id', 'type']);

        });

        \App\MqttMessage::truncate();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mqtt_messages', function (Blueprint $table) {
            //
        });
    }
}
